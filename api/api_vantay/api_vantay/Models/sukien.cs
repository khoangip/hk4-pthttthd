//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace api_vantay.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SuKien
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SuKien()
        {
            this.MaMayThamGiaSuKiens = new HashSet<MaMayThamGiaSuKien>();
            this.ThongTinThamGiaSuKiens = new HashSet<ThongTinThamGiaSuKien>();
        }
    
        public int SuKienID { get; set; }
        public string TenSuKien { get; set; }
        public string MoTa { get; set; }
        public System.DateTime ThoiGianBatDau { get; set; }
        public System.DateTime ThoiGianKetThuc { get; set; }
        public int SoNguoiToiDa { get; set; }
        public int SoNguoiThamGia { get; set; }
        public System.DateTime NgayTao { get; set; }
        public System.DateTime NgayCapNhat { get; set; }
        public System.DateTime NgayBatDau { get; set; }
        public System.DateTime NgayKetThuc { get; set; }
        public int PhongID { get; set; }
        public Nullable<int> NguoiPhuTrachID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MaMayThamGiaSuKien> MaMayThamGiaSuKiens { get; set; }
        public virtual NguoiDung NguoiDung { get; set; }
        public virtual Phong Phong { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThongTinThamGiaSuKien> ThongTinThamGiaSuKiens { get; set; }
    }
}
