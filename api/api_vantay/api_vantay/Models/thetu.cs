//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace api_vantay.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TheTu
    {
        public int TheTuID { get; set; }
        public int NguoiDungID { get; set; }
        public string VanTay1 { get; set; }
        public string VanTay2 { get; set; }
        public string VanTay3 { get; set; }
        public bool TrangThai { get; set; }
        public System.DateTime NgayTao { get; set; }
        public System.DateTime NgayCapNhat { get; set; }
    
        public virtual NguoiDung NguoiDung { get; set; }
    }
}
