//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace api_vantay.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MaMayThamGiaSuKien
    {
        public int SuKienID { get; set; }
        public int MayQuetTheVanTayID { get; set; }
        public int TrangThai { get; set; }
    
        public virtual MayQuetTheVanTay MayQuetTheVanTay { get; set; }
        public virtual SuKien SuKien { get; set; }
    }
}
