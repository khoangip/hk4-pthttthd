﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using api_vantay.Models;

namespace api_vantay.Controllers
{
    public class nguoidungsController : ApiController
    {
        private quetvantayEntities db = new quetvantayEntities();

        // GET: api/nguoidungs
        [HttpGet]
        [Route("api/nguoidungs")]
        public HttpResponseMessage Getnguoidung()
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings
    .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters
                .Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            var res = Request.CreateResponse(HttpStatusCode.OK, db.nguoidung);
            res.Headers.Add("Access-Control-Allow-Origin", "*");
            return res;
        }

        // GET: api/nguoidungs/5
        [ResponseType(typeof(nguoidung))]
        public IHttpActionResult Getnguoidung(int id)
        {
            nguoidung nguoidung = db.nguoidung.Find(id);
            if (nguoidung == null)
            {
                return NotFound();
            }

            return Ok(nguoidung);
        }

        // PUT: api/nguoidungs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putnguoidung(int id, nguoidung nguoidung)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nguoidung.NguoiDungID)
            {
                return BadRequest();
            }

            db.Entry(nguoidung).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!nguoidungExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/nguoidungs
        [ResponseType(typeof(nguoidung))]
        public IHttpActionResult Postnguoidung(nguoidung nguoidung)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.nguoidung.Add(nguoidung);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = nguoidung.NguoiDungID }, nguoidung);
        }

        // DELETE: api/nguoidungs/5
        [ResponseType(typeof(nguoidung))]
        public IHttpActionResult Deletenguoidung(int id)
        {
            nguoidung nguoidung = db.nguoidung.Find(id);
            if (nguoidung == null)
            {
                return NotFound();
            }

            db.nguoidung.Remove(nguoidung);
            db.SaveChanges();

            return Ok(nguoidung);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool nguoidungExists(int id)
        {
            return db.nguoidung.Count(e => e.NguoiDungID == id) > 0;
        }
    }
}