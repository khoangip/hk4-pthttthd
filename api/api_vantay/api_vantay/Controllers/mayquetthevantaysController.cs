﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using api_vantay.Models;

namespace api_vantay.Controllers
{
    public class mayquetthevantaysController : ApiController
    {
        private quetvantayEntities db = new quetvantayEntities();

        // GET: api/mayquetthevantays
        [HttpGet]
        [Route("api/mayquetthevantays")]
        public HttpResponseMessage Getmayquetthevantay()
        {

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings
    .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters
                .Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);


            var res = Request.CreateResponse(HttpStatusCode.OK, db.mayquetthevantay);
            res.Headers.Add("Access-Control-Allow-Origin", "*");
            res.Headers.Add("Access-Control-Allow-Methods", "*");
            res.Headers.Add("Access-Control-Allow-Headers", "*");
            return res;
        }

        // GET: api/mayquetthevantays/5
        [ResponseType(typeof(mayquetthevantay))]
        public IHttpActionResult Getmayquetthevantay(int id)
        {
            mayquetthevantay mayquetthevantay = db.mayquetthevantay.Find(id);
            if (mayquetthevantay == null)
            {
                return NotFound();
            }

            return Ok(mayquetthevantay);
        }

        // PUT: api/mayquetthevantays/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putmayquetthevantay(int id, mayquetthevantay mayquetthevantay)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mayquetthevantay.MayQuetTheVanTayID)
            {
                return BadRequest();
            }

            db.Entry(mayquetthevantay).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!mayquetthevantayExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/mayquetthevantays
        [ResponseType(typeof(mayquetthevantay))]
        public IHttpActionResult Postmayquetthevantay(mayquetthevantay mayquetthevantay)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.mayquetthevantay.Add(mayquetthevantay);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mayquetthevantay.MayQuetTheVanTayID }, mayquetthevantay);
        }

        // DELETE: api/mayquetthevantays/5
        [ResponseType(typeof(mayquetthevantay))]
        public IHttpActionResult Deletemayquetthevantay(int id)
        {
            mayquetthevantay mayquetthevantay = db.mayquetthevantay.Find(id);
            if (mayquetthevantay == null)
            {
                return NotFound();
            }

            db.mayquetthevantay.Remove(mayquetthevantay);
            db.SaveChanges();

            return Ok(mayquetthevantay);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool mayquetthevantayExists(int id)
        {
            return db.mayquetthevantay.Count(e => e.MayQuetTheVanTayID == id) > 0;
        }
    }
}