﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api_vantay.Controllers
{
    public class SimpleApiController : ApiController
    {
        [HttpGet]
        [Route("api/simple/say")]
        public HttpResponseMessage SayHello()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Hello");
        }
    }
}
