﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiemDanhVanTay.Models;
using System.Data.Entity;

namespace DiemDanhVanTay.DAO
{
    public class SuKienRepository : ISuKienRepository, IDisposable
    {
        private DiemDanhDbContext context;

        public SuKienRepository(DiemDanhDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<SuKien> GetSuKien()
        {
            return context.SuKiens.ToList();
        }

        public SuKien GetSuKienByID(int suKienId)
        {
            return context.SuKiens.Find(suKienId);
        }

        public void InsertSuKien(SuKien suKien)
        {
            context.SuKiens.Add(suKien);
        }

        public void DeleteSuKien(int suKienId)
        {
            SuKien suKien = context.SuKiens.Find(suKienId);
            context.SuKiens.Remove(suKien);
        }

        public void UpdateSuKien(SuKien suKien)
        {
            context.Entry(suKien).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}