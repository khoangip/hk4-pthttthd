﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiemDanhVanTay.Models;
using System.Data.Entity;

namespace DiemDanhVanTay.DAO
{
    public class LoaiNguoiDungRepository : ILoaiNguoiDungRepository, IDisposable
    {
        private DiemDanhDbContext context;

        public LoaiNguoiDungRepository(DiemDanhDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<LoaiNguoiDung> GetLoaiNguoiDung()
        {
            return context.LoaiNguoiDungs.ToList();
        }

        public LoaiNguoiDung GetLoaiNguoiDungByID(int loaiNguoiDungId)
        {
            return context.LoaiNguoiDungs.Find(loaiNguoiDungId);
        }

        public void InsertLoaiNguoiDung(LoaiNguoiDung loaiNguoiDung)
        {
            context.LoaiNguoiDungs.Add(loaiNguoiDung);
        }

        public void DeleteLoaiNguoiDung(int loaiNguoiDungId)
        {
            LoaiNguoiDung loaiNguoiDung = context.LoaiNguoiDungs.Find(loaiNguoiDungId);
            context.LoaiNguoiDungs.Remove(loaiNguoiDung);
        }

        public void UpdateLoaiNguoiDung(LoaiNguoiDung loaiNguoiDung)
        {
            context.Entry(loaiNguoiDung).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}