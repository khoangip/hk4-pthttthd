﻿using DiemDanhVanTay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.DAO
{
    public interface IMayQuetTheVanTayRepository : IDisposable
    {
        IEnumerable<MayQuetTheVanTay> GetMayQuetTheVanTay();
        MayQuetTheVanTay GetMayQuetTheVanTayByID(int mayQuetTheVanTayId);
        void InsertMayQuetTheVanTay(MayQuetTheVanTay mayQuetTheVanTay);
        void DeleteMayQuetTheVanTay(int mayQuetTheVanTayId);
        void UpdateMayQuetTheVanTay(MayQuetTheVanTay mayQuetTheVanTay);
        void Save();
    }
}