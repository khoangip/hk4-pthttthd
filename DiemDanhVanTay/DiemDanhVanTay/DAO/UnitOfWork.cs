﻿using DiemDanhVanTay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.DAO
{
    public class UnitOfWork : IDisposable
    {
        private DiemDanhDbContext context = new DiemDanhDbContext();
        private GenericRepository<LoaiNguoiDung> loaiNguoiDungRepository;
        private GenericRepository<NguoiDung> nguoiDungRepository;
        private GenericRepository<SuKien> suKienRepository;
        private GenericRepository<MayQuetTheVanTay> mayQuetTheVanTayRepository;
        private GenericRepository<MaMayThamGiaSuKien> maMayThamGiaSuKienRepository;
        private GenericRepository<TheTu> theTuRepository;
        private GenericRepository<ThongTinThamGiaSuKien> thongTinThamGiaSuKienRepository;
        private GenericRepository<Phong> phongRepository;

        public GenericRepository<LoaiNguoiDung> LoaiNguoiDungRepository
        {
            get
            {
                if (this.loaiNguoiDungRepository == null)
                {
                    this.loaiNguoiDungRepository = new GenericRepository<LoaiNguoiDung>(context);
                }
                return loaiNguoiDungRepository;
            }
        }

        public GenericRepository<Phong> PhongRepository
        {
            get
            {
                if (this.phongRepository == null)
                {
                    this.phongRepository = new GenericRepository<Phong>(context);
                }
                return phongRepository;
            }
        }

        public GenericRepository<NguoiDung> NguoiDungRepository
        {
            get
            {
                if (this.nguoiDungRepository == null)
                {
                    this.nguoiDungRepository = new GenericRepository<NguoiDung>(context);
                }
                return nguoiDungRepository;
            }
        }

        public GenericRepository<SuKien> SuKienRepository
        {
            get
            {
                if (this.suKienRepository == null)
                {
                    this.suKienRepository = new GenericRepository<SuKien>(context);
                }
                return suKienRepository;
            }
        }

        public GenericRepository<MayQuetTheVanTay> MayQuetTheVanTayRepository
        {
            get
            {
                if (this.mayQuetTheVanTayRepository == null)
                {
                    this.mayQuetTheVanTayRepository = new GenericRepository<MayQuetTheVanTay>(context);
                }
                return mayQuetTheVanTayRepository;
            }
        }

        public GenericRepository<MaMayThamGiaSuKien> MaMayThamGiaSuKienRepository
        {
            get
            {
                if (this.maMayThamGiaSuKienRepository == null)
                {
                    this.maMayThamGiaSuKienRepository = new GenericRepository<MaMayThamGiaSuKien>(context);
                }
                return maMayThamGiaSuKienRepository;
            }
        }

        public GenericRepository<TheTu> TheTuRepository
        {
            get
            {
                if (this.theTuRepository == null)
                {
                    this.theTuRepository = new GenericRepository<TheTu>(context);
                }
                return theTuRepository;
            }
        }

        public GenericRepository<ThongTinThamGiaSuKien> ThongTinThamGiaSuKienRepository
        {
            get
            {
                if (this.thongTinThamGiaSuKienRepository == null)
                {
                    this.thongTinThamGiaSuKienRepository = new GenericRepository<ThongTinThamGiaSuKien>(context);
                }
                return thongTinThamGiaSuKienRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}