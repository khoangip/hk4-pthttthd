﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.DAO
{
    public class DiemDanhDbContext : DbContext
    {
        public DiemDanhDbContext() : base("DiemDanhDbContext")
        {
        }

        public DbSet<LoaiNguoiDung> LoaiNguoiDungs { get; set; }

        public DbSet<NguoiDung> NguoiDungs { get; set; }

        public DbSet<TheTu> TheTus { get; set; }

        public DbSet<SuKien> SuKiens { get; set; }

        public DbSet<MayQuetTheVanTay> MayQuetTheVanTays { get; set; }

        public DbSet<MaMayThamGiaSuKien> MaMayThamGiaSuKiens { get; set; }

        public DbSet<ThongTinThamGiaSuKien> ThongTinThamGiaSuKiens { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public System.Data.Entity.DbSet<DiemDanhVanTay.Models.Phong> Phongs { get; set; }
    }
}