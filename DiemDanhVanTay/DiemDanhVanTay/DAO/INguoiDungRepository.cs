﻿using DiemDanhVanTay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.DAO
{
    public interface INguoiDungRepository: IDisposable
    {
        IEnumerable<NguoiDung> GetNguoiDung();
        NguoiDung GetNguoiDungByID(int nguoiDungId);
        void InsertNguoiDung(NguoiDung nguoiDung);
        void DeleteNguoiDung(int nguoiDungId);
        void UpdateNguoiDung(NguoiDung nguoiDung);
        void Save();
    }
}