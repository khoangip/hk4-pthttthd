﻿using DiemDanhVanTay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.DAO
{
    public interface IMaMayThamGiaSuKienRepository: IDisposable
    {
        IEnumerable<MaMayThamGiaSuKien> GetMaMayThamGiaSuKien();
        MaMayThamGiaSuKien GetMaMayThamGiaSuKienByID(object maMayThamGiaSuKienId);
        void InsertMaMayThamGiaSuKien(MaMayThamGiaSuKien maMayThamGiaSuKien);
        void DeleteMaMayThamGiaSuKien(object maMayThamGiaSuKienId);
        void UpdateMaMayThamGiaSuKien(MaMayThamGiaSuKien MaMayThamGiaSuKien);
        void Save();
    }
}