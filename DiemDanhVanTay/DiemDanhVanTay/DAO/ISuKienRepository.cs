﻿using DiemDanhVanTay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.DAO
{
    public interface ISuKienRepository : IDisposable
    {
        IEnumerable<SuKien> GetSuKien();
        SuKien GetSuKienByID(int suKienId);
        void InsertSuKien(SuKien suKien);
        void DeleteSuKien(int suKienId);
        void UpdateSuKien(SuKien suKien);
        void Save();
    }
}