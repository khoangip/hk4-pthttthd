﻿using DiemDanhVanTay.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.DAO
{
    public class NguoiDungRepository : INguoiDungRepository, IDisposable
    {
        private DiemDanhDbContext context;

        public NguoiDungRepository(DiemDanhDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<NguoiDung> GetNguoiDung()
        {
            return context.NguoiDungs.ToList();
        }

        public NguoiDung GetNguoiDungByID(int nguoiDungId)
        {
            return context.NguoiDungs.Find(nguoiDungId);
        }

        public void InsertNguoiDung(NguoiDung nguoiDung)
        {
            context.NguoiDungs.Add(nguoiDung);
        }

        public void DeleteNguoiDung(int nguoiDungId)
        {
            NguoiDung nguoiDung = context.NguoiDungs.Find(nguoiDungId);
            context.NguoiDungs.Remove(nguoiDung);
        }

        public void UpdateNguoiDung(NguoiDung nguoiDung)
        {
            context.Entry(nguoiDung).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}