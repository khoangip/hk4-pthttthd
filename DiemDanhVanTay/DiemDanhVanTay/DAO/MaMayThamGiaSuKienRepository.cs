﻿using DiemDanhVanTay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.DAO
{
    public class MaMayThamGiaSuKienRepository : IMaMayThamGiaSuKienRepository, IDisposable
    {
        public void DeleteMaMayThamGiaSuKien(object maMayThamGiaSuKienId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MaMayThamGiaSuKien> GetMaMayThamGiaSuKien()
        {
            throw new NotImplementedException();
        }

        public MaMayThamGiaSuKien GetMaMayThamGiaSuKienByID(object maMayThamGiaSuKienId)
        {
            throw new NotImplementedException();
        }

        public void InsertMaMayThamGiaSuKien(MaMayThamGiaSuKien maMayThamGiaSuKien)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void UpdateMaMayThamGiaSuKien(MaMayThamGiaSuKien MaMayThamGiaSuKien)
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~MaMayThamGiaSuKienRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}