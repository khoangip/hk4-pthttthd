﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiemDanhVanTay.Models;
using System.Data.Entity;

namespace DiemDanhVanTay.DAO
{
    public class MayQuetTheVanTayRepository : IMayQuetTheVanTayRepository, IDisposable
    {
        private DiemDanhDbContext context;

        public MayQuetTheVanTayRepository(DiemDanhDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<MayQuetTheVanTay> GetMayQuetTheVanTay()
        {
            return context.MayQuetTheVanTays.ToList();
        }

        public MayQuetTheVanTay GetMayQuetTheVanTayByID(int mayQuetTheVanTayId)
        {
            return context.MayQuetTheVanTays.Find(mayQuetTheVanTayId);
        }

        public void InsertMayQuetTheVanTay(MayQuetTheVanTay mayQuetTheVanTay)
        {
            context.MayQuetTheVanTays.Add(mayQuetTheVanTay);
        }

        public void DeleteMayQuetTheVanTay(int mayQuetTheVanTayId)
        {
            MayQuetTheVanTay mayQuetTheVanTay = context.MayQuetTheVanTays.Find(mayQuetTheVanTayId);
            context.MayQuetTheVanTays.Remove(mayQuetTheVanTay);
        }

        public void UpdateMayQuetTheVanTay(MayQuetTheVanTay mayQuetTheVanTay)
        {
            context.Entry(mayQuetTheVanTay).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}