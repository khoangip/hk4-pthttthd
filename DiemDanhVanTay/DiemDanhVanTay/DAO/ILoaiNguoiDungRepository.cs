﻿using DiemDanhVanTay.Models;
using System;
using System.Collections.Generic;

namespace DiemDanhVanTay.DAO
{
    public interface ILoaiNguoiDungRepository: IDisposable
    {
        IEnumerable<LoaiNguoiDung> GetLoaiNguoiDung();
        LoaiNguoiDung GetLoaiNguoiDungByID(int loaiNguoiDungId);
        void InsertLoaiNguoiDung(LoaiNguoiDung loaiNguoiDung);
        void DeleteLoaiNguoiDung(int loaiNguoiDungId);
        void UpdateLoaiNguoiDung(LoaiNguoiDung loaiNguoiDung);
        void Save();
    }
}