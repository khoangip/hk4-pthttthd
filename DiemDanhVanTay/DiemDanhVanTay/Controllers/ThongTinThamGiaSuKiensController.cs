﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class ThongTinThamGiaSuKiensController : ApiController
    {
        private DiemDanhDbContext db = new DiemDanhDbContext();
        private String error;
        private DateTime dateError;
        private String stateOfDiemDanh;
        private String nameOfUser;

        [HttpGet]
        [Route("api/ThongTinThamGiaSuKiens/check")]
        public HttpResponseMessage reciveInfoThamGia(String ma, String mamay)
        {
            dateError = DateTime.MinValue;
            bool checkSuKien = false;

            // check van tay co trong db ko
            var infoTheTu = this.getUserFromVanTay(ma);
            var infoSuKien = this.getSukienFromMaMay(mamay);

            if (!checkString(this.error) && (infoSuKien != null && infoTheTu != null)) // nếu ko có error thì thực hiện
            {
                checkSuKien = this.checkNguoiDungCoTrongSuKienKhong(infoSuKien.SuKienID, infoTheTu.NguoiDungID);
                this.nameOfUser = infoTheTu.HoTen;
            }
            // check error + trả kết quả
            Returntype returnType = new Returntype();
            if (checkString(this.error) || checkSuKien == false)
            {
                returnType.Error = 1;
                returnType.reason = this.error;
            }
            else
            {
                returnType.Error = 0;
                returnType.reason = this.nameOfUser + " điểm danh " + this.stateOfDiemDanh + " thành công";
            }
            var res = Request.CreateResponse(HttpStatusCode.OK, returnType);
            res.Headers.Add("Access-Control-Allow-Origin", "*");
            return res;
        }
        public bool updateByMaSuKienMaNguoiDung(int maSuKien, int maNguoiDung)
        {
            // update
            var result = db.ThongTinThamGiaSuKiens.SingleOrDefault(b => b.NguoiDungID == maNguoiDung && b.SuKienID == maSuKien);
            if (result != null)
            {
                result.TrangThai = 1;
                result.LaGiangVien = false;
                int checkinout = this.checkInorOut(result);

                if (checkinout == 0) result.ThoiGianVao = DateTime.Now;
                else if (checkinout == 1) result.ThoiGianRa = DateTime.Now;
                else if (checkinout == 2)
                {
                    this.error += "|| bạn đã điểm danh rồi";
                    return false;
                }
                else
                {
                    this.error += "|| unknow action";
                    return false;
                }
                db.SaveChanges();
            }
            else
            {
                this.error += "|| masukien='" + maSuKien + "' manguoidung='" + maNguoiDung + "' ko tim thay su kien";
                return false;
            }
            db.SaveChanges();
            return true;
        }
        /** nếu chưa có thoi gian vao thì vào , 
         * nếu đã có thời gian vào thì ra
         * 
         * return 0 là vào
         *  return 1 là ra
         *  return 2 là đã điểm danh goy
         */
        public int checkInorOut(ThongTinThamGiaSuKien tttgsk)
        {
            if (tttgsk.ThoiGianVao == null)
            {
                this.stateOfDiemDanh = "vào";
                return 0;
            }
            if (tttgsk.ThoiGianRa == null)
            {
                this.stateOfDiemDanh = "ra";
                return 1;
            }
            this.error += "|| đã điểm danh rồi";
            return 2;
        }
        public bool checkNguoiDungCoTrongSuKienKhong(int maSuKien, int maNguoiDung)
        {
            int err = 0;
            if (maSuKien == 0)
            {
                this.error += "|| masukien null "; err = 1;
            }
            if (maNguoiDung == 0)
            {
                this.error += "|| ma người dùng null "; err = 1;
            }
            if (err == 1) return false;
            var infoCheck = db.ThongTinThamGiaSuKiens.Where(c => c.NguoiDungID == maNguoiDung && c.SuKienID == maSuKien).FirstOrDefault();
            if (infoCheck == null)
            {
                this.error += "|| chưa lấy đc su kiện";
                return false;
            }
            //addByMaSuKienMaNguoiDung(maSuKien, maNguoiDung);
            return updateByMaSuKienMaNguoiDung(maSuKien, maNguoiDung);
            //return true;
        }
        public SuKien getSukienFromMaMay(String mamay)
        {
            if (!checkString(mamay))
            {
                this.error += "||mã máy '" + mamay + "' chưa hợp lệ";
            }
            var dateNow = DateTime.Now;
            var infoMaMay = db.MaMayThamGiaSuKiens.Where(c => c.MayQuetTheVanTayID.ToString() == mamay && c.TrangThai == 1).ToList();

            if (infoMaMay == null)
            {
                this.error += "|| chưa tìm thấy sự kiện của mã máy này";
                return null;
            }
            foreach (var mmRow in infoMaMay)
            {
                // lay thong tin su kien 
                var rowSuKien = db.SuKiens.Where(c => c.SuKienID == mmRow.SuKienID).FirstOrDefault();
                if (rowSuKien == null)
                {
                    this.error += "|| ko tìm thấy mã sự kiện " + mmRow.SuKienID + " tại table mamythamgiasukien ";
                }
                else
                {
                    var traSuKien = this.getSuKienNow(rowSuKien);
                    if (traSuKien == null) continue;
                    return traSuKien;
                }
            }
            this.error += "|| chưa tìm thấy  sự kiện";
            return null;
        }
        /** return null nếu lỗi
         * return sukien neu ok */
        public SuKien getSuKienNow(SuKien rowSuKien)
        {
            //conver datetime? to datetime
            int err = 0;
            DateTime rowNgayBatDau = rowSuKien.NgayBatDau;
            DateTime rowGioBatDau = rowSuKien.ThoiGianBatDau;
            DateTime rowGioKetThuc = rowSuKien.ThoiGianKetThuc;
            DateTime dateNow = DateTime.Now;
            if (rowSuKien.NgayBatDau == null)
            {
                this.error += "|| ngay bat dau ko dc null";
                err = 1;
            }
            if (rowSuKien.ThoiGianBatDau == null)
            {
                this.error += "|| thoi gian bat dau ko dc null";
                err = 1;
            }
            if (rowSuKien.ThoiGianKetThuc == null)
            {
                this.error += "|| thoi gian ket thuc ko dc null";
                err = 1;
            }
            if (err == 1) return null;
            if (rowSuKien.NgayKetThuc != null)
            {
                DateTime rowNgayKetThuc = rowSuKien.NgayKetThuc;
                if (dateNow.Date >= rowNgayBatDau.Date && dateNow.Date <= rowNgayKetThuc.Date)
                {
                    if (dateNow.Hour >= rowGioBatDau.Hour && dateNow.Hour <= rowGioKetThuc.Hour)
                    {
                        return rowSuKien;
                    }
                }
                return null;
            }
            if (rowNgayBatDau.Date == dateNow.Date)
            {
                // nếu ngày hiện tại đúng là sự kiện này check giờ
                if (dateNow.Hour >= rowGioBatDau.Hour && dateNow.Hour <= rowGioKetThuc.Hour)
                {
                    return rowSuKien;
                }
            }
            return null;
        }
        public NguoiDung getUserFromVanTay(String ma)
        {
            if (!checkString(ma))
            {
                this.error += "||mã vân tay '" + ma + "' chưa hợp lệ";
                return null;
            }
            var infoMa = db.TheTus.Where(c => c.VanTay1 == ma || c.VanTay2 == ma || c.VanTay3 == ma).FirstOrDefault();
            if (infoMa == null)
            {
                this.error += "||mã vân tay '" + ma + "' ko tìm thấy trong database";
                return null;
            }
            // lấy thong tin nguoi dung
            var infoNguoiDung = db.NguoiDungs.Where(c => c.NguoiDungID == infoMa.NguoiDungID).FirstOrDefault();
            if (infoNguoiDung == null)
            {
                this.error += "|| chưa lấy đc info từ mã người dùng này";
            }
            return infoNguoiDung;
        }
        /** trả về true nếu chuỗi đưa vào hợp lệ ( ko có khoảng trắng , ko rỗng ko null ) và ngược lại
         * @var String chuỗi đưa vào
         * @return boolean
        */
        public Boolean checkString(String str)
        {
            if (String.IsNullOrEmpty(str) || String.IsNullOrWhiteSpace(str))
            {
                return false;
            }
            return true;
        }
    }
}