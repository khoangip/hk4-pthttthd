﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class MayQuetTheVanTayController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: MayQuetTheVanTay
        public ActionResult Index()
        {
            var mayQuetTheVanTays = unitOfWork.MayQuetTheVanTayRepository.Get();
            return View(mayQuetTheVanTays.ToList());
        }

        // GET: MayQuetTheVanTay/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MayQuetTheVanTay mayQuetTheVanTay = unitOfWork.MayQuetTheVanTayRepository.GetByID(id);
            if (mayQuetTheVanTay == null)
            {
                return HttpNotFound();
            }
            return View(mayQuetTheVanTay);
        }

        // GET: MayQuetTheVanTay/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MayQuetTheVanTay/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MayQuetTheVanTayID,TenMay,MoTa")] MayQuetTheVanTay mayQuetTheVanTay)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.MayQuetTheVanTayRepository.Insert(mayQuetTheVanTay);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }           
            return View(mayQuetTheVanTay);
        }

        // GET: MayQuetTheVanTay/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MayQuetTheVanTay mayQuetTheVanTay = unitOfWork.MayQuetTheVanTayRepository.GetByID(id);
            if (mayQuetTheVanTay == null)
            {
                return HttpNotFound();
            }
            return View(mayQuetTheVanTay);
        }

        // POST: MayQuetTheVanTay/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MayQuetTheVanTayID,TenMay,MoTa")] MayQuetTheVanTay mayQuetTheVanTay)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.MayQuetTheVanTayRepository.Update(mayQuetTheVanTay);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }         
            return View(mayQuetTheVanTay);
        }

        // GET: MayQuetTheVanTay/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MayQuetTheVanTay mayQuetTheVanTay = unitOfWork.MayQuetTheVanTayRepository.GetByID(id);
            if (mayQuetTheVanTay == null)
            {
                return HttpNotFound();
            }
            return View(mayQuetTheVanTay);
        }

        // POST: MayQuetTheVanTay/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                MayQuetTheVanTay mayQuetTheVanTay = unitOfWork.MayQuetTheVanTayRepository.GetByID(id);
                unitOfWork.MayQuetTheVanTayRepository.Delete(mayQuetTheVanTay);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }           
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
