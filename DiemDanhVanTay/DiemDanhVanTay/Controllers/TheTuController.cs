﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class TheTuController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: TheTu
        public ActionResult Index()
        {
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV");
            var theTus = unitOfWork.TheTuRepository.Get(includeProperties: "NguoiDung");
            CreateAndIndexTheTuModel model = new CreateAndIndexTheTuModel();
            TheTu theTu = new TheTu();
            model.TheTus = theTus.ToList();
            model.TheTu = theTu;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "NguoiDungID, TrangThai")] TheTu theTu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    theTu.NgayTao = DateTime.Now;
                    theTu.NgayCapNhat = DateTime.Now;
                    unitOfWork.TheTuRepository.Insert(theTu);
                    unitOfWork.Save();
                    ViewBag.InsertError = 0;
                    return RedirectToAction("Index");
                }        
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }

            ViewBag.InsertError = 1;
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV", theTu.NguoiDungID);
            var theTus = unitOfWork.TheTuRepository.Get(includeProperties: "NguoiDung");
            CreateAndIndexTheTuModel model = new CreateAndIndexTheTuModel();
            model.TheTus = theTus.ToList();
            model.TheTu = theTu;
            return View(model);
        }

        // GET: TheTu/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TheTu theTu = unitOfWork.TheTuRepository.GetByID(id);
            if (theTu == null)
            {
                return HttpNotFound();
            }
            return View(theTu);
        }

        // GET: TheTu/Create
        public ActionResult Create()
        {
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV");
            return View();
        }

        // POST: TheTu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TheTuID,NguoiDungID,VanTay1,VanTay2,VanTay3,TrangThai,NgayTao,NgayCapNhat")] TheTu theTu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    theTu.NgayTao = DateTime.Now;
                    theTu.NgayCapNhat = DateTime.Now;
                    unitOfWork.TheTuRepository.Insert(theTu);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }         
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV", theTu.NguoiDungID);
            return View(theTu);
        }

        // GET: TheTu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TheTu theTu = unitOfWork.TheTuRepository.GetByID(id);
            if (theTu == null)
            {
                return HttpNotFound();
            }
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV", theTu.NguoiDungID);
            return View(theTu);
        }

        // POST: TheTu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TheTuID,NguoiDungID,VanTay1,VanTay2,VanTay3,TrangThai,NgayTao")] TheTu theTu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    theTu.NgayCapNhat = DateTime.Now;
                    unitOfWork.TheTuRepository.Update(theTu);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }            
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV", theTu.NguoiDungID);
            return View(theTu);
        }

        // GET: TheTu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TheTu theTu = unitOfWork.TheTuRepository.GetByID(id);
            if (theTu == null)
            {
                return HttpNotFound();
            }
            return View(theTu);
        }

        // POST: TheTu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                TheTu theTu = unitOfWork.TheTuRepository.GetByID(id);
                unitOfWork.TheTuRepository.Delete(theTu);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }          
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Save();
            base.Dispose(disposing);
        }
    }
}
