﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class NguoiDungController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: NguoiDung
        public ActionResult Index()
        {
            ViewBag.LoaiNguoiDungID = new SelectList(unitOfWork.LoaiNguoiDungRepository.Get(), "LoaiNguoiDungID", "TenLoaiNguoiDung");
            var nguoiDungs = unitOfWork.NguoiDungRepository.Get(includeProperties: "LoaiNguoiDung");
            CreateAndIndexNguoiDungModel model = new CreateAndIndexNguoiDungModel();
            NguoiDung nguoiDung = new NguoiDung();
            nguoiDung.TrangThai = true;
            model.NguoiDungs = nguoiDungs.ToList();
            model.NguoiDung = nguoiDung;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "NguoiDungID,LoaiNguoiDungID,HoTen,NgaySinh,CMND,DiaChi,DienThoai,Email,TrangThai")] NguoiDung nguoiDung)
        {
            try
            {
                nguoiDung.MatKhau = nguoiDung.CMND;
                nguoiDung.NgayTao = DateTime.Now;
                nguoiDung.NgayCapNhat = DateTime.Now;
                unitOfWork.NguoiDungRepository.Insert(nguoiDung);
                unitOfWork.Save();
                nguoiDung.MSSV = (DateTime.Now.Year * 10000 + nguoiDung.NguoiDungID).ToString();
                unitOfWork.NguoiDungRepository.Update(nguoiDung);
                unitOfWork.Save();

                ViewBag.InsertError = 0;
                return RedirectToAction("Index");
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }

            ViewBag.InsertError = 1;
            ViewBag.LoaiNguoiDungID = new SelectList(unitOfWork.LoaiNguoiDungRepository.Get(), "LoaiNguoiDungID", "TenLoaiNguoiDung", nguoiDung.LoaiNguoiDungID);
            var nguoiDungs = unitOfWork.NguoiDungRepository.Get(includeProperties: "LoaiNguoiDung");
            CreateAndIndexNguoiDungModel model = new CreateAndIndexNguoiDungModel();
            model.NguoiDungs = nguoiDungs.ToList();
            model.NguoiDung = nguoiDung;
            return View(model);
        }

        // GET: NguoiDung/Details/5
        public ActionResult Details(int id)
        {

            NguoiDung nguoiDung = unitOfWork.NguoiDungRepository.GetByID(id);
            if (nguoiDung == null)
            {
                return HttpNotFound();
            }
            //from s in unitOfWork.NguoiDungRepository.Get(includeProperties: "SuKien") select s).Where(s => s.NguoiDungID == suKien.NguoiPhuTrachID
            var suKiens = (from s in unitOfWork.ThongTinThamGiaSuKienRepository.Get(includeProperties: "SuKien") select s).Where(s => s.NguoiDungID == nguoiDung.NguoiDungID).ToList();
            var tuple = new Tuple<NguoiDung, IEnumerable<ThongTinThamGiaSuKien>>(nguoiDung, suKiens);
            return View(tuple);
        }

        // GET: NguoiDung/Create
        public ActionResult Create()
        {
            ViewBag.LoaiNguoiDungID = new SelectList(unitOfWork.LoaiNguoiDungRepository.Get(), "LoaiNguoiDungID", "TenLoaiNguoiDung");
            return View();
        }

        // POST: NguoiDung/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NguoiDungID,LoaiNguoiDungID,MatKhau,HoTen,NgaySinh,CMND,DiaChi,DienThoai,Email,NgayTao,NgayCapNhat")] NguoiDung nguoiDung)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    nguoiDung.NgayTao = DateTime.Now;
                    nguoiDung.NgayCapNhat = DateTime.Now;
                    unitOfWork.NguoiDungRepository.Insert(nguoiDung);
                    unitOfWork.Save();
                    nguoiDung.MSSV = (DateTime.Now.Year * 10000 + nguoiDung.NguoiDungID).ToString();
                    unitOfWork.NguoiDungRepository.Update(nguoiDung);
                    unitOfWork.Save();

                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }

            ViewBag.LoaiNguoiDungID = new SelectList(unitOfWork.LoaiNguoiDungRepository.Get(), "LoaiNguoiDungID", "TenLoaiNguoiDung", nguoiDung.LoaiNguoiDungID);
            return View(nguoiDung);
        }

        // GET: NguoiDung/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NguoiDung nguoiDung = unitOfWork.NguoiDungRepository.GetByID(id);
            if (nguoiDung == null)
            {
                return HttpNotFound();
            }
            ViewBag.LoaiNguoiDungID = new SelectList(unitOfWork.LoaiNguoiDungRepository.Get(), "LoaiNguoiDungID", "TenLoaiNguoiDung", nguoiDung.LoaiNguoiDungID);
            return View(nguoiDung);
        }

        // POST: NguoiDung/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NguoiDungID,LoaiNguoiDungID,MatKhau,HoTen,NgaySinh,CMND,DiaChi,DienThoai,Email,NgayTao,NgayCapNhat,TrangThai,MSSV")] NguoiDung nguoiDung)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    nguoiDung.NgayCapNhat = DateTime.Now;
                    unitOfWork.NguoiDungRepository.Update(nguoiDung);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }

            ViewBag.LoaiNguoiDungID = new SelectList(unitOfWork.LoaiNguoiDungRepository.Get(), "LoaiNguoiDungID", "TenLoaiNguoiDung", nguoiDung.LoaiNguoiDungID);
            return View(nguoiDung);
        }

        // GET: NguoiDung/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NguoiDung nguoiDung = unitOfWork.NguoiDungRepository.GetByID(id);
            if (nguoiDung == null)
            {
                return HttpNotFound();
            }
            return View(nguoiDung);
        }

        // POST: NguoiDung/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                NguoiDung nguoiDung = unitOfWork.NguoiDungRepository.GetByID(id);
                unitOfWork.NguoiDungRepository.Delete(id);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
        }

        public ActionResult DetailJson(int id)
        {
            NguoiDung nguoiDung = unitOfWork.NguoiDungRepository.GetByID(id);
            return Json(nguoiDung, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
