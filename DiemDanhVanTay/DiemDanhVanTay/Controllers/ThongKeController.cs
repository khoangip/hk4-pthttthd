﻿using DiemDanhVanTay.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiemDanhVanTay.Controllers
{
    public class ThongKeController : Controller
    {
        // GET: ThongKe

        private UnitOfWork unitOfWork = new UnitOfWork();

        public ActionResult Index(int? suKienID)
        {
            var diemDanhs = unitOfWork.ThongTinThamGiaSuKienRepository.Get(includeProperties: "NguoiDung");
            if (suKienID != null)
            {
                diemDanhs = diemDanhs.Where(s => s.SuKienID == suKienID);
                ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien", suKienID);
            }
            else
            {
                ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien");
            }
            return View(diemDanhs.ToList());
        }
    }
}