﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class TheTusController : ApiController
    {
        private DiemDanhDbContext db = new DiemDanhDbContext();

        // GET: api/TheTus
        [HttpGet]
        [Route("api/TheTus/getthetu")]
        public HttpResponseMessage Getnguoidung()
        {
            //db.TheTus
            //System.Collections.ArrayList returnInfo = new System.Collections.ArrayList();
            var infothetu = db.TheTus.Select( x => new {x.NguoiDung.HoTen, x.NguoiDung.LoaiNguoiDung.TenLoaiNguoiDung ,x.VanTay1} )
                .ToList();
            var res = Request.CreateResponse(HttpStatusCode.OK, infothetu );
            res.Headers.Add("Access-Control-Allow-Origin", "*");
            return res;
        }

        // GET: api/TheTus/5
        [ResponseType(typeof(TheTu))]
        public IHttpActionResult GetTheTu(int id)
        {
            TheTu theTu = db.TheTus.Find(id);
            if (theTu == null)
            {
                return NotFound();
            }

            return Ok(theTu);
        }

        // PUT: api/TheTus/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTheTu(int id, TheTu theTu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != theTu.TheTuID)
            {
                return BadRequest();
            }

            db.Entry(theTu).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TheTuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TheTus
        [ResponseType(typeof(TheTu))]
        public IHttpActionResult PostTheTu(TheTu theTu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TheTus.Add(theTu);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = theTu.TheTuID }, theTu);
        }

        // DELETE: api/TheTus/5
        [ResponseType(typeof(TheTu))]
        public IHttpActionResult DeleteTheTu(int id)
        {
            TheTu theTu = db.TheTus.Find(id);
            if (theTu == null)
            {
                return NotFound();
            }

            db.TheTus.Remove(theTu);
            db.SaveChanges();

            return Ok(theTu);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TheTuExists(int id)
        {
            return db.TheTus.Count(e => e.TheTuID == id) > 0;
        }
    }
}