﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class LoaiNguoiDungController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: LoaiNguoiDung
        public ActionResult Index()
        {
            var loaiNguoiDungs = unitOfWork.LoaiNguoiDungRepository.Get();
            return View(loaiNguoiDungs.ToList());
        }


        // GET: LoaiNguoiDung/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            LoaiNguoiDung loaiNguoiDung = unitOfWork.LoaiNguoiDungRepository.GetByID(id);
            if (loaiNguoiDung == null)
            {
                return HttpNotFound();
            }
            return View(loaiNguoiDung);
        }

        // POST: LoaiNguoiDung/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LoaiNguoiDungID,TenLoaiNguoiDung,TrangThai")] LoaiNguoiDung loaiNguoiDung)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    loaiNguoiDung.TrangThai = Convert.ToInt32(Request["accountType"]);
                    unitOfWork.LoaiNguoiDungRepository.Update(loaiNguoiDung);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }

            return View(loaiNguoiDung);
        }

       
        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
