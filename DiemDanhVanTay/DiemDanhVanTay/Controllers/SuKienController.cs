﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class SuKienController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: SuKien
        public ActionResult Index()
        {
            var suKiens = unitOfWork.SuKienRepository.Get(includeProperties: "Phong,NguoiDung");
            return View(suKiens.ToList());
        }

        // GET: SuKien/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuKien suKien = unitOfWork.SuKienRepository.GetByID(id);
            
            var tuple = new Tuple<SuKien, IEnumerable<ThongTinThamGiaSuKien>>(null, null);
            var sinhviens = (from s in unitOfWork.ThongTinThamGiaSuKienRepository.Get(includeProperties: "NguoiDung") select s).Where(s => s.SuKienID == 0).ToList(); ;
            var giaovien = new NguoiDung();
            if (suKien == null)
            {
                return HttpNotFound();
            }
            else
            {
                sinhviens = (from s in unitOfWork.ThongTinThamGiaSuKienRepository.Get(includeProperties: "NguoiDung") select s).Where(s=> s.SuKienID == suKien.SuKienID).ToList();

                giaovien = (from s in unitOfWork.NguoiDungRepository.Get() select s).Where(s => s.NguoiDungID == suKien.NguoiPhuTrachID).First();
                
                tuple = new Tuple<SuKien, IEnumerable<ThongTinThamGiaSuKien>>(suKien, sinhviens);

            }
            ViewBag.SinhViens = sinhviens;
            ViewBag.giaovien = giaovien;
            return View(tuple);
        }

        // GET: SuKien/Create
        public ActionResult Create()
        {
            var giaoVien = unitOfWork.NguoiDungRepository.Get();
            giaoVien = giaoVien.Where(g => g.LoaiNguoiDungID == 2);
            ViewBag.NguoiPhuTrachID = new SelectList(giaoVien, "NguoiDungID", "HoTen");
            ViewBag.PhongID = new SelectList(unitOfWork.PhongRepository.Get(), "PhongID", "TenPhong");
            return View();
        }

        // POST: SuKien/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SuKienID,TenSuKien,MoTa,NgayBatDau,NgayKetThuc,ThoiGianBatDau,ThoiGianKetThuc,SoNguoiToiDa,SoNguoiThamGia,NgayTao,NgayCapNhat,TrangThai,PhongID,NguoiPhuTrachID")] SuKien suKien)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    suKien.NgayTao = DateTime.Now;
                    suKien.NgayCapNhat = DateTime.Now;
                    unitOfWork.SuKienRepository.Insert(suKien);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }
            var giaoVien = unitOfWork.NguoiDungRepository.Get();
            giaoVien = giaoVien.Where(g => g.LoaiNguoiDungID == 2);
            ViewBag.NguoiPhuTrachID = new SelectList(giaoVien, "NguoiDungID", "HoTen", suKien.NguoiPhuTrachID);
            ViewBag.PhongID = new SelectList(unitOfWork.PhongRepository.Get(), "PhongID", "TenPhong", suKien.PhongID);
            return View(suKien);
        }

        // GET: SuKien/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuKien suKien = unitOfWork.SuKienRepository.GetByID(id);
            if (suKien == null)
            {
                return HttpNotFound();
            }
            var giaoVien = unitOfWork.NguoiDungRepository.Get(); 
            giaoVien = giaoVien.Where(g => g.LoaiNguoiDungID == 2);
            ViewBag.NguoiPhuTrachID = new SelectList(giaoVien, "NguoiDungID", "HoTen", suKien.NguoiPhuTrachID);
            ViewBag.PhongID = new SelectList(unitOfWork.PhongRepository.Get(), "PhongID", "TenPhong", suKien.PhongID);
            return View(suKien);
        }

        // POST: SuKien/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SuKienID,TenSuKien,MoTa,NgayBatDau,NgayKetThuc,ThoiGianBatDau,ThoiGianKetThuc,SoNguoiToiDa,SoNguoiThamGia,NgayTao,NgayCapNhat,TrangThai,PhongID,NguoiPhuTrachID")] SuKien suKien)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    suKien.NgayCapNhat = DateTime.Now;
                    unitOfWork.SuKienRepository.Update(suKien);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }
            var giaoVien = unitOfWork.NguoiDungRepository.Get();
            giaoVien = giaoVien.Where(g => g.LoaiNguoiDungID == 2);
            ViewBag.NguoiPhuTrachID = new SelectList(giaoVien, "NguoiDungID", "HoTen", suKien.NguoiPhuTrachID);
            ViewBag.PhongID = new SelectList(unitOfWork.PhongRepository.Get(), "PhongID", "TenPhong", suKien.PhongID);
            return View(suKien);
        }

        // GET: SuKien/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuKien suKien = unitOfWork.SuKienRepository.GetByID(id);
            if (suKien == null)
            {
                return HttpNotFound();
            }
            return View(suKien);
        }

        // POST: SuKien/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                SuKien suKien = unitOfWork.SuKienRepository.GetByID(id);
                unitOfWork.SuKienRepository.Delete(id);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
