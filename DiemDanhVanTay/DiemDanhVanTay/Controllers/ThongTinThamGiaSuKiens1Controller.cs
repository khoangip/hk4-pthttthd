﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class ThongTinThamGiaSuKiens1Controller : Controller
    {
        private DiemDanhDbContext db = new DiemDanhDbContext();

        // GET: ThongTinThamGiaSuKiens1
        public ActionResult Index()
        {
            var thongTinThamGiaSuKiens = db.ThongTinThamGiaSuKiens.Include(t => t.NguoiDung).Include(t => t.SuKien);
            return View(thongTinThamGiaSuKiens.ToList());
        }

        // GET: ThongTinThamGiaSuKiens1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThongTinThamGiaSuKien thongTinThamGiaSuKien = db.ThongTinThamGiaSuKiens.Find(id);
            if (thongTinThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            return View(thongTinThamGiaSuKien);
        }

        // GET: ThongTinThamGiaSuKiens1/Create
        public ActionResult Create()
        {
            ViewBag.NguoiDungID = new SelectList(db.NguoiDungs, "NguoiDungID", "MSSV");
            ViewBag.SuKienID = new SelectList(db.SuKiens, "SuKienID", "TenSuKien");
            return View();
        }

        // POST: ThongTinThamGiaSuKiens1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SuKienID,NguoiDungID,TrangThai,ThoiGianVao,ThoiGianRa,LaGiangVien")] ThongTinThamGiaSuKien thongTinThamGiaSuKien)
        {
            if (ModelState.IsValid)
            {
                db.ThongTinThamGiaSuKiens.Add(thongTinThamGiaSuKien);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NguoiDungID = new SelectList(db.NguoiDungs, "NguoiDungID", "MSSV", thongTinThamGiaSuKien.NguoiDungID);
            ViewBag.SuKienID = new SelectList(db.SuKiens, "SuKienID", "TenSuKien", thongTinThamGiaSuKien.SuKienID);
            return View(thongTinThamGiaSuKien);
        }

        // GET: ThongTinThamGiaSuKiens1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThongTinThamGiaSuKien thongTinThamGiaSuKien = db.ThongTinThamGiaSuKiens.Find(id);
            if (thongTinThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            ViewBag.NguoiDungID = new SelectList(db.NguoiDungs, "NguoiDungID", "MSSV", thongTinThamGiaSuKien.NguoiDungID);
            ViewBag.SuKienID = new SelectList(db.SuKiens, "SuKienID", "TenSuKien", thongTinThamGiaSuKien.SuKienID);
            return View(thongTinThamGiaSuKien);
        }

        // POST: ThongTinThamGiaSuKiens1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SuKienID,NguoiDungID,TrangThai,ThoiGianVao,ThoiGianRa,LaGiangVien")] ThongTinThamGiaSuKien thongTinThamGiaSuKien)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thongTinThamGiaSuKien).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NguoiDungID = new SelectList(db.NguoiDungs, "NguoiDungID", "MSSV", thongTinThamGiaSuKien.NguoiDungID);
            ViewBag.SuKienID = new SelectList(db.SuKiens, "SuKienID", "TenSuKien", thongTinThamGiaSuKien.SuKienID);
            return View(thongTinThamGiaSuKien);
        }

        // GET: ThongTinThamGiaSuKiens1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThongTinThamGiaSuKien thongTinThamGiaSuKien = db.ThongTinThamGiaSuKiens.Find(id);
            if (thongTinThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            return View(thongTinThamGiaSuKien);
        }

        // POST: ThongTinThamGiaSuKiens1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ThongTinThamGiaSuKien thongTinThamGiaSuKien = db.ThongTinThamGiaSuKiens.Find(id);
            db.ThongTinThamGiaSuKiens.Remove(thongTinThamGiaSuKien);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
