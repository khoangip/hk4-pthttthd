﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class MayQuetTheVanTaysController : ApiController
    {
        private DiemDanhDbContext db = new DiemDanhDbContext();

        // GET: api/MayQuetTheVanTays
        [HttpGet]
        [Route("api/MayQuetTheVanTays")]
        public HttpResponseMessage Getnguoidung()
        {
            var res = Request.CreateResponse(HttpStatusCode.OK, db.MayQuetTheVanTays);
            res.Headers.Add("Access-Control-Allow-Origin", "*");
            return res;
        }

        // GET: api/MayQuetTheVanTays/5
        [ResponseType(typeof(MayQuetTheVanTay))]
        public IHttpActionResult GetMayQuetTheVanTay(int id)
        {
            MayQuetTheVanTay mayQuetTheVanTay = db.MayQuetTheVanTays.Find(id);
            if (mayQuetTheVanTay == null)
            {
                return NotFound();
            }

            return Ok(mayQuetTheVanTay);
        }

        // PUT: api/MayQuetTheVanTays/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMayQuetTheVanTay(int id, MayQuetTheVanTay mayQuetTheVanTay)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mayQuetTheVanTay.MayQuetTheVanTayID)
            {
                return BadRequest();
            }

            db.Entry(mayQuetTheVanTay).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MayQuetTheVanTayExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MayQuetTheVanTays
        [ResponseType(typeof(MayQuetTheVanTay))]
        public IHttpActionResult PostMayQuetTheVanTay(MayQuetTheVanTay mayQuetTheVanTay)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MayQuetTheVanTays.Add(mayQuetTheVanTay);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mayQuetTheVanTay.MayQuetTheVanTayID }, mayQuetTheVanTay);
        }

        // DELETE: api/MayQuetTheVanTays/5
        [ResponseType(typeof(MayQuetTheVanTay))]
        public IHttpActionResult DeleteMayQuetTheVanTay(int id)
        {
            MayQuetTheVanTay mayQuetTheVanTay = db.MayQuetTheVanTays.Find(id);
            if (mayQuetTheVanTay == null)
            {
                return NotFound();
            }

            db.MayQuetTheVanTays.Remove(mayQuetTheVanTay);
            db.SaveChanges();

            return Ok(mayQuetTheVanTay);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MayQuetTheVanTayExists(int id)
        {
            return db.MayQuetTheVanTays.Count(e => e.MayQuetTheVanTayID == id) > 0;
        }
    }
}