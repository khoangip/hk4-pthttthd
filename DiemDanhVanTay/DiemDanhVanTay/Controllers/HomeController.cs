﻿using DiemDanhVanTay.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiemDanhVanTay.Controllers
{
    public class HomeController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public ActionResult Index()
        {
            var diemDanhs = unitOfWork.ThongTinThamGiaSuKienRepository.Get(includeProperties: "NguoiDung");
            ViewBag.SuKienList = unitOfWork.SuKienRepository.Get();
            return View(diemDanhs.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}