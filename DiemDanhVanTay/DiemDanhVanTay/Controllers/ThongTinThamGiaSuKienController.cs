﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class ThongTinThamGiaSuKienController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: ThongTinThamGiaSuKien
        public ActionResult Index()
        {
            var thongTinThamGiaSuKiens = unitOfWork.ThongTinThamGiaSuKienRepository.Get(includeProperties: "SuKien, NguoiDung");
            return View(thongTinThamGiaSuKiens.ToList());
        }

        // GET: ThongTinThamGiaSuKien/Details/5
        public ActionResult Details(int? suKienId, int? nguoiDungId)
        {
            if (suKienId == null || nguoiDungId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThongTinThamGiaSuKien thongTinThamGiaSuKien = unitOfWork.ThongTinThamGiaSuKienRepository.GetByID(suKienId, nguoiDungId);
            if (thongTinThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            return View(thongTinThamGiaSuKien);
        }

        // GET: ThongTinThamGiaSuKien/Create
        public ActionResult Create()
        {
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV");
            ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien");
            return View();
        }

        // POST: ThongTinThamGiaSuKien/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SuKienID,NguoiDungID,TrangThai,ThoiGianVao,ThoiGianRa,LaGiangVien")] ThongTinThamGiaSuKien thongTinThamGiaSuKien)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.ThongTinThamGiaSuKienRepository.Insert(thongTinThamGiaSuKien);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV", thongTinThamGiaSuKien.NguoiDungID);
            ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien", thongTinThamGiaSuKien.SuKienID);
            return View(thongTinThamGiaSuKien);
        }

        // GET: ThongTinThamGiaSuKien/Edit/5
        public ActionResult Edit(int? suKienId, int? nguoiDungId)
        {
            if (suKienId == null || nguoiDungId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThongTinThamGiaSuKien thongTinThamGiaSuKien = unitOfWork.ThongTinThamGiaSuKienRepository.GetByID(suKienId, nguoiDungId);
            if (thongTinThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV", thongTinThamGiaSuKien.NguoiDungID);
            ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien", thongTinThamGiaSuKien.SuKienID);
            return View(thongTinThamGiaSuKien);
        }

        // POST: ThongTinThamGiaSuKien/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SuKienID,NguoiDungID,TrangThai,ThoiGianVao,ThoiGianRa,LaGiangVien")] ThongTinThamGiaSuKien thongTinThamGiaSuKien)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.ThongTinThamGiaSuKienRepository.Update(thongTinThamGiaSuKien);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }
            ViewBag.NguoiDungID = new SelectList(unitOfWork.NguoiDungRepository.Get(), "NguoiDungID", "MSSV", thongTinThamGiaSuKien.NguoiDungID);
            ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien", thongTinThamGiaSuKien.SuKienID);
            return View(thongTinThamGiaSuKien);
        }

        // GET: ThongTinThamGiaSuKien/Delete/5
        public ActionResult Delete(int? suKienId, int? nguoiDungId)
        {
            if (suKienId == null || nguoiDungId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThongTinThamGiaSuKien thongTinThamGiaSuKien = unitOfWork.ThongTinThamGiaSuKienRepository.GetByID(suKienId, nguoiDungId);
            if (thongTinThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            return View(thongTinThamGiaSuKien);
        }

        // POST: ThongTinThamGiaSuKien/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int suKienId, int nguoiDungId)
        {
            try
            {
                ThongTinThamGiaSuKien thongTinThamGiaSuKien = unitOfWork.ThongTinThamGiaSuKienRepository.GetByID(suKienId, nguoiDungId);
                unitOfWork.ThongTinThamGiaSuKienRepository.Delete(thongTinThamGiaSuKien);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                return RedirectToAction("Delete", new { suKienId = suKienId, nguoiDungId = nguoiDungId, saveChangesError = true });
            }
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
