﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class MaMayThamGiaSuKienController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: MaMayThamGiaSuKien
        public ActionResult Index()
        {
            var maMayThamGiaSuKiens = unitOfWork.MaMayThamGiaSuKienRepository.Get(includeProperties: "SuKien,MayQuetTheVanTay");
            return View(maMayThamGiaSuKiens.ToList());
        }

        // GET: MaMayThamGiaSuKien/Details/5
        public ActionResult Details(int? suKienId, int? mayQuetId)
        {
            if (suKienId == null || mayQuetId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaMayThamGiaSuKien maMayThamGiaSuKien = unitOfWork.MaMayThamGiaSuKienRepository.GetByID(suKienId, mayQuetId);
            if (maMayThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            return View(maMayThamGiaSuKien);
        }

        // GET: MaMayThamGiaSuKien/Create
        public ActionResult Create()
        {
            ViewBag.MayQuetTheVanTayID = new SelectList(unitOfWork.MayQuetTheVanTayRepository.Get(), "MayQuetTheVanTayID", "TenMay");
            ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien");
            return View();
        }

        // POST: MaMayThamGiaSuKien/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SuKienID,MayQuetTheVanTayID,TrangThai")] MaMayThamGiaSuKien maMayThamGiaSuKien)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.MaMayThamGiaSuKienRepository.Insert(maMayThamGiaSuKien);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            ViewBag.MayQuetTheVanTayID = new SelectList(unitOfWork.MayQuetTheVanTayRepository.Get(), "MayQuetTheVanTayID", "TenMay", maMayThamGiaSuKien.MayQuetTheVanTayID);
            ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien", maMayThamGiaSuKien.SuKienID);
            return View(maMayThamGiaSuKien);
        }

        // GET: MaMayThamGiaSuKien/Edit/5
        public ActionResult Edit(int? suKienId, int? mayQuetId)
        {
            if (suKienId == null || mayQuetId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaMayThamGiaSuKien maMayThamGiaSuKien = unitOfWork.MaMayThamGiaSuKienRepository.GetByID(suKienId, mayQuetId);
            if (maMayThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            ViewBag.MayQuetTheVanTayID = new SelectList(unitOfWork.MayQuetTheVanTayRepository.Get(), "MayQuetTheVanTayID", "TenMay", maMayThamGiaSuKien.MayQuetTheVanTayID);
            ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien", maMayThamGiaSuKien.SuKienID);
            return View(maMayThamGiaSuKien);
        }

        // POST: MaMayThamGiaSuKien/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SuKienID,MayQuetTheVanTayID,TrangThai")] MaMayThamGiaSuKien maMayThamGiaSuKien)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.MaMayThamGiaSuKienRepository.Update(maMayThamGiaSuKien);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }
            ViewBag.MayQuetTheVanTayID = new SelectList(unitOfWork.MayQuetTheVanTayRepository.Get(), "MayQuetTheVanTayID", "TenMay", maMayThamGiaSuKien.MayQuetTheVanTayID);
            ViewBag.SuKienID = new SelectList(unitOfWork.SuKienRepository.Get(), "SuKienID", "TenSuKien", maMayThamGiaSuKien.SuKienID);
            return View(maMayThamGiaSuKien);
        }

        // GET: MaMayThamGiaSuKien/Delete/5
        public ActionResult Delete(int? suKienId, int? mayQuetId)
        {
            if (suKienId == null || mayQuetId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaMayThamGiaSuKien maMayThamGiaSuKien = unitOfWork.MaMayThamGiaSuKienRepository.GetByID(suKienId, mayQuetId);
            if (maMayThamGiaSuKien == null)
            {
                return HttpNotFound();
            }
            return View(maMayThamGiaSuKien);
        }

        // POST: MaMayThamGiaSuKien/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int suKienId, int mayQuetId)
        {
            try
            {
                MaMayThamGiaSuKien maMayThamGiaSuKien = unitOfWork.MaMayThamGiaSuKienRepository.GetByID(suKienId, mayQuetId);
                unitOfWork.MaMayThamGiaSuKienRepository.Delete(maMayThamGiaSuKien);
                unitOfWork.Save();
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                return RedirectToAction("Delete", new { suKienId = suKienId, mayQuetId = mayQuetId, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
