﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DiemDanhVanTay.DAO;
using DiemDanhVanTay.Models;

namespace DiemDanhVanTay.Controllers
{
    public class NguoiDungsController : ApiController
    {
        private DiemDanhDbContext db = new DiemDanhDbContext();

        // GET: api/nguoidungs
        [HttpGet]
        [Route("api/NguoiDungs/Getnguoidung")]
        public HttpResponseMessage Getnguoidung()
        {
            var res = Request.CreateResponse(HttpStatusCode.OK, db.NguoiDungs);
            res.Headers.Add("Access-Control-Allow-Origin", "*");
            return res;
        }

        // GET: api/NguoiDungs/5
        [ResponseType(typeof(NguoiDung))]
        public IHttpActionResult GetNguoiDung(int id)
        {
            NguoiDung nguoiDung = db.NguoiDungs.Find(id);
            if (nguoiDung == null)
            {
                return NotFound();
            }

            return Ok(nguoiDung);
        }

        // PUT: api/NguoiDungs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNguoiDung(int id, NguoiDung nguoiDung)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nguoiDung.NguoiDungID)
            {
                return BadRequest();
            }

            db.Entry(nguoiDung).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NguoiDungExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/NguoiDungs
        [ResponseType(typeof(NguoiDung))]
        public IHttpActionResult PostNguoiDung(NguoiDung nguoiDung)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.NguoiDungs.Add(nguoiDung);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = nguoiDung.NguoiDungID }, nguoiDung);
        }

        // DELETE: api/NguoiDungs/5
        [ResponseType(typeof(NguoiDung))]
        public IHttpActionResult DeleteNguoiDung(int id)
        {
            NguoiDung nguoiDung = db.NguoiDungs.Find(id);
            if (nguoiDung == null)
            {
                return NotFound();
            }

            db.NguoiDungs.Remove(nguoiDung);
            db.SaveChanges();

            return Ok(nguoiDung);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NguoiDungExists(int id)
        {
            return db.NguoiDungs.Count(e => e.NguoiDungID == id) > 0;
        }
    }
}