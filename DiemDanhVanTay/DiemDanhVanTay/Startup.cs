﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DiemDanhVanTay.Startup))]
namespace DiemDanhVanTay
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
