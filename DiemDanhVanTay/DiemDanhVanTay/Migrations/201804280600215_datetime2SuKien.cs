namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datetime2SuKien : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SuKien", "ThoiGianBatDau", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SuKien", "ThoiGianKetThuc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SuKien", "ThoiGianKetThuc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SuKien", "ThoiGianBatDau", c => c.DateTime(nullable: false));
        }
    }
}
