namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoaiNguoiDung",
                c => new
                    {
                        LoaiNguoiDungID = c.Int(nullable: false, identity: true),
                        TenLoaiNguoiDung = c.String(nullable: false),
                        TrangThai = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LoaiNguoiDungID);
            
            CreateTable(
                "dbo.MaMayThamGiaSuKien",
                c => new
                    {
                        SuKienID = c.Int(nullable: false),
                        MayQuetTheVanTayID = c.Int(nullable: false),
                        TrangThai = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SuKienID, t.MayQuetTheVanTayID })
                .ForeignKey("dbo.MayQuetTheVanTay", t => t.MayQuetTheVanTayID, cascadeDelete: true)
                .ForeignKey("dbo.SuKien", t => t.SuKienID, cascadeDelete: true)
                .Index(t => t.SuKienID)
                .Index(t => t.MayQuetTheVanTayID);
            
            CreateTable(
                "dbo.MayQuetTheVanTay",
                c => new
                    {
                        MayQuetTheVanTayID = c.Int(nullable: false, identity: true),
                        TenMay = c.String(nullable: false),
                        MoTa = c.String(),
                    })
                .PrimaryKey(t => t.MayQuetTheVanTayID);
            
            CreateTable(
                "dbo.SuKien",
                c => new
                    {
                        SuKienID = c.Int(nullable: false, identity: true),
                        TenSuKien = c.String(nullable: false),
                        MoTa = c.String(),
                        ThoiGianBatDau = c.DateTime(nullable: false),
                        ThoiGianKetThuc = c.DateTime(nullable: false),
                        SoNguoiToiDa = c.Int(nullable: false),
                        SoNguoiThamGia = c.Int(nullable: false),
                        NgayTao = c.DateTime(nullable: false),
                        NgayCapNhat = c.DateTime(nullable: false),
                        TrangThai = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SuKienID);
            
            CreateTable(
                "dbo.NguoiDung",
                c => new
                    {
                        NguoiDungID = c.Int(nullable: false, identity: true),
                        LoaiNguoiDungID = c.Int(nullable: false),
                        MSSV = c.String(nullable: false),
                        MatKhau = c.String(),
                        HoTen = c.String(nullable: false, maxLength: 50),
                        NgaySinh = c.DateTime(nullable: false),
                        CMND = c.String(nullable: false),
                        DiaChi = c.String(nullable: false),
                        DienThoai = c.String(),
                        Email = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        NgayCapNhat = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.NguoiDungID)
                .ForeignKey("dbo.LoaiNguoiDung", t => t.LoaiNguoiDungID, cascadeDelete: true)
                .Index(t => t.LoaiNguoiDungID);
            
            CreateTable(
                "dbo.TheTu",
                c => new
                    {
                        TheTuID = c.Int(nullable: false, identity: true),
                        NguoiDungID = c.Int(nullable: false),
                        VanTay1 = c.String(),
                        VanTay2 = c.String(),
                        VanTay3 = c.String(),
                        TrangThai = c.Int(nullable: false),
                        NgayTao = c.DateTime(nullable: false),
                        NgayCapNhat = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TheTuID)
                .ForeignKey("dbo.NguoiDung", t => t.NguoiDungID, cascadeDelete: true)
                .Index(t => t.NguoiDungID);
            
            CreateTable(
                "dbo.ThongTinThamGiaSuKien",
                c => new
                    {
                        SuKienID = c.Int(nullable: false),
                        NguoiDungID = c.Int(nullable: false),
                        TrangThai = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SuKienID, t.NguoiDungID })
                .ForeignKey("dbo.NguoiDung", t => t.NguoiDungID, cascadeDelete: true)
                .ForeignKey("dbo.SuKien", t => t.SuKienID, cascadeDelete: true)
                .Index(t => t.SuKienID)
                .Index(t => t.NguoiDungID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ThongTinThamGiaSuKien", "SuKienID", "dbo.SuKien");
            DropForeignKey("dbo.ThongTinThamGiaSuKien", "NguoiDungID", "dbo.NguoiDung");
            DropForeignKey("dbo.TheTu", "NguoiDungID", "dbo.NguoiDung");
            DropForeignKey("dbo.NguoiDung", "LoaiNguoiDungID", "dbo.LoaiNguoiDung");
            DropForeignKey("dbo.MaMayThamGiaSuKien", "SuKienID", "dbo.SuKien");
            DropForeignKey("dbo.MaMayThamGiaSuKien", "MayQuetTheVanTayID", "dbo.MayQuetTheVanTay");
            DropIndex("dbo.ThongTinThamGiaSuKien", new[] { "NguoiDungID" });
            DropIndex("dbo.ThongTinThamGiaSuKien", new[] { "SuKienID" });
            DropIndex("dbo.TheTu", new[] { "NguoiDungID" });
            DropIndex("dbo.NguoiDung", new[] { "LoaiNguoiDungID" });
            DropIndex("dbo.MaMayThamGiaSuKien", new[] { "MayQuetTheVanTayID" });
            DropIndex("dbo.MaMayThamGiaSuKien", new[] { "SuKienID" });
            DropTable("dbo.ThongTinThamGiaSuKien");
            DropTable("dbo.TheTu");
            DropTable("dbo.NguoiDung");
            DropTable("dbo.SuKien");
            DropTable("dbo.MayQuetTheVanTay");
            DropTable("dbo.MaMayThamGiaSuKien");
            DropTable("dbo.LoaiNguoiDung");
        }
    }
}
