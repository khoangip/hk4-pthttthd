namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeTrangThaiInSuKien : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.SuKien", "TrangThai");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SuKien", "TrangThai", c => c.Int(nullable: false));
        }
    }
}
