namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPhong : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Phong",
                c => new
                    {
                        PhongID = c.Int(nullable: false, identity: true),
                        TenPhong = c.String(nullable: false),
                        SoChoNgoi = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PhongID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Phong");
        }
    }
}
