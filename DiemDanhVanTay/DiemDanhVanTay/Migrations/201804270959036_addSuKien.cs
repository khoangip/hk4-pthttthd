namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSuKien : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SuKien", "NgayBatDau", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.SuKien", "NgayKetThuc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SuKien", "NgayKetThuc");
            DropColumn("dbo.SuKien", "NgayBatDau");
        }
    }
}
