namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeThongTinThamGiaSuKien : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ThongTinThamGiaSuKien", "LaGiangVien", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ThongTinThamGiaSuKien", "LaGiangVien");
        }
    }
}
