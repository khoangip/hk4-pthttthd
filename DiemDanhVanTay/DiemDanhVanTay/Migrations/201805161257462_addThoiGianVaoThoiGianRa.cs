namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addThoiGianVaoThoiGianRa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ThongTinThamGiaSuKien", "ThoiGianVao", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.ThongTinThamGiaSuKien", "ThoiGianRa", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ThongTinThamGiaSuKien", "ThoiGianRa");
            DropColumn("dbo.ThongTinThamGiaSuKien", "ThoiGianVao");
        }
    }
}
