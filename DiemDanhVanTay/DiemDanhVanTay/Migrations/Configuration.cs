﻿namespace DiemDanhVanTay.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DiemDanhVanTay.DAO.DiemDanhDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DiemDanhVanTay.DAO.DiemDanhDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var loaiNguoiDungs = new List<LoaiNguoiDung>
            {
                new LoaiNguoiDung { LoaiNguoiDungID = 1, TenLoaiNguoiDung = "Admin", TrangThai = 1 },
                new LoaiNguoiDung { LoaiNguoiDungID = 2, TenLoaiNguoiDung = "Giáo viên", TrangThai = 1 },
                new LoaiNguoiDung { LoaiNguoiDungID = 3, TenLoaiNguoiDung = "Học sinh", TrangThai = 1 }
            };

            var nguoiDungs = new List<NguoiDung>
            {
                new NguoiDung
                {
                    NguoiDungID = 1,
                    LoaiNguoiDungID = 1,
                    MSSV = "20180001",
                    MatKhau = "123456",
                    HoTen = "Admin",
                    NgaySinh = Convert.ToDateTime("4/27/2018 12:00:00 AM"),
                    CMND = "123456789",
                    DiaChi = "DH KHTN",
                    DienThoai = "0909090009",
                    Email = "admin@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 2,
                    LoaiNguoiDungID = 2,
                    MSSV = "20180002",
                    MatKhau = "123456",
                    HoTen = "Trần Quốc Nguyên",
                    NgaySinh = Convert.ToDateTime("5/12/1980 12:00:00 AM"),
                    CMND = "234567890",
                    DiaChi = "432 Nguyễn Thị Minh Khai, Phường 5, Quận 3, Hồ Chí Minh",
                    DienThoai = "0908090808",
                    Email = "tqnguyen@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 3,
                    LoaiNguoiDungID = 3,
                    MSSV = "20180003",
                    MatKhau = "123456",
                    HoTen = "Lưu Đặng Trung Hiếu",
                    NgaySinh = Convert.ToDateTime("12/12/1992 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "1 Lê Thị Riêng, Phường Thới An, Quận 12, Thành Phố Hồ Chí Minh",
                    DienThoai = "0909090909",
                    Email = "ldthieu@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 4,
                    LoaiNguoiDungID = 3,
                    MSSV = "20180004",
                    MatKhau = "123456",
                    HoTen = "Vũ Quốc Khánh",
                    NgaySinh = Convert.ToDateTime("12/12/1994 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "ngã tư Thủ Đức",
                    DienThoai = "0909091009",
                    Email = "vqkhanh@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 5,
                    LoaiNguoiDungID = 3,
                    MSSV = "20180005",
                    MatKhau = "123456",
                    HoTen = "Nguyễn Trương Phi",
                    NgaySinh = Convert.ToDateTime("01/01/1995 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "Viện Y Dược TP.HCM",
                    DienThoai = "0909000009",
                    Email = "ntphi@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 6,
                    LoaiNguoiDungID = 3,
                    MSSV = "20180006",
                    MatKhau = "123456",
                    HoTen = "Trần Tiết Thịnh",
                    NgaySinh = Convert.ToDateTime("01/01/1995 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "Q.8 TP.HCM",
                    DienThoai = "0909000010",
                    Email = "ttthinh@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 7,
                    LoaiNguoiDungID = 3,
                    MSSV = "20180007",
                    MatKhau = "123456",
                    HoTen = "Lưu Thế Nguyên",
                    NgaySinh = Convert.ToDateTime("01/01/1995 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "Quân khu 7",
                    DienThoai = "0909000011",
                    Email = "ltnguyen@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 8,
                    LoaiNguoiDungID = 2,
                    MSSV = "20180008",
                    MatKhau = "123456",
                    HoTen = "Dương Tấn Huỳnh Phong",
                    NgaySinh = Convert.ToDateTime("01/01/1995 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "Quận 7 TP.HCM",
                    DienThoai = "0909000011",
                    Email = "dthphong@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 9,
                    LoaiNguoiDungID = 2,
                    MSSV = "20180009",
                    MatKhau = "123456",
                    HoTen = "Huỳnh Duy Thức",
                    NgaySinh = Convert.ToDateTime("01/01/1994 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "Ông Ích Khiêm, Q.11, TP.HCM",
                    DienThoai = "0909000011",
                    Email = "hdthuc@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = true
                },
                new NguoiDung
                {
                    NguoiDungID = 10,
                    LoaiNguoiDungID = 3,
                    MSSV = "20180010",
                    MatKhau = "123456",
                    HoTen = "Tạ Quốc Thịnh",
                    NgaySinh = Convert.ToDateTime("01/02/1995 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "Phạm Văn Chiêu, Gò Vấp",
                    DienThoai = "0909011111",
                    Email = "tqthinh@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = false
                },
                new NguoiDung
                {
                    NguoiDungID = 11,
                    LoaiNguoiDungID = 3,
                    MSSV = "20180011",
                    MatKhau = "123456",
                    HoTen = "Trần Văn Lâm",
                    NgaySinh = Convert.ToDateTime("01/03/1995 09:00:00 PM"),
                    CMND = "345678901",
                    DiaChi = "Q10, TP.HCM",
                    DienThoai = "0909111111",
                    Email = "tvlam@gmail.com",
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                    TrangThai = false
                },
            };

            var theTus = new List<TheTu>
            {
                new TheTu
                {
                    TheTuID = 1,
                    NguoiDungID = 1,
                    VanTay1 = "vantay01user01",
                    VanTay2 = "vantay02user01",
                    VanTay3 = "vantay03user01",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                },
                new TheTu
                {
                    TheTuID = 2,
                    NguoiDungID = 2,
                    VanTay1 = "vantay01user02",
                    VanTay2 = "vantay02user02",
                    VanTay3 = "vantay03user02",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                },
                new TheTu
                {
                    TheTuID = 3,
                    NguoiDungID = 3,
                    VanTay1 = "vantay01user03",
                    VanTay2 = "vantay02user03",
                    VanTay3 = "vantay03user03",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                },
                new TheTu
                {
                    TheTuID = 4,
                    NguoiDungID = 4,
                    VanTay1 = "vantay01user04",
                    VanTay2 = "vantay02user04",
                    VanTay3 = "vantay03user04",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                },
                new TheTu
                {
                    TheTuID = 5,
                    NguoiDungID = 5,
                    VanTay1 = "vantay01user05",
                    VanTay2 = "vantay02user05",
                    VanTay3 = "vantay03user05",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                },
                new TheTu
                {
                    TheTuID = 6,
                    NguoiDungID = 6,
                    VanTay1 = "vantay01user06",
                    VanTay2 = "vantay02user06",
                    VanTay3 = "vantay03user06",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                },
                new TheTu
                {
                    TheTuID = 7,
                    NguoiDungID = 7,
                    VanTay1 = "vantay01user07",
                    VanTay2 = "vantay02user07",
                    VanTay3 = "vantay03user07",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                },
                new TheTu
                {
                    TheTuID = 8,
                    NguoiDungID = 8,
                    VanTay1 = "vantay01user08",
                    VanTay2 = "vantay02user08",
                    VanTay3 = "vantay03user08",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                },
                new TheTu
                {
                    TheTuID = 9,
                    NguoiDungID = 9,
                    VanTay1 = "vantay01user09",
                    VanTay2 = "vantay02user09",
                    VanTay3 = "vantay03user09",
                    TrangThai = true,
                    NgayTao = DateTime.Now,
                    NgayCapNhat = DateTime.Now,
                }
            };

            var phongs = new List<Phong>
            {
                new Phong{ PhongID = 1, TenPhong = "Hội trường 1", SoChoNgoi = 400 },
                new Phong{ PhongID = 2, TenPhong = "Hội trường 2", SoChoNgoi = 400 },
                new Phong{ PhongID = 3, TenPhong = "C203", SoChoNgoi = 100 },
                new Phong{ PhongID = 4, TenPhong = "C403", SoChoNgoi = 400 },
                new Phong{ PhongID = 5, TenPhong = "F311", SoChoNgoi = 100 },
                new Phong{ PhongID = 6, TenPhong = "B40a", SoChoNgoi = 30 },
                new Phong{ PhongID = 7, TenPhong = "B40b", SoChoNgoi = 40 },
                new Phong{ PhongID = 8, TenPhong = "I603", SoChoNgoi = 50 }
            };

            var mays = new List<MayQuetTheVanTay>
            {
                new MayQuetTheVanTay { MayQuetTheVanTayID = 1, TenMay = "MQT01", MoTa = "Máy quét thẻ 01" },
                new MayQuetTheVanTay { MayQuetTheVanTayID = 2, TenMay = "MQT02", MoTa = "Máy quét thẻ 02" },
                new MayQuetTheVanTay { MayQuetTheVanTayID = 3, TenMay = "MQT03", MoTa = "Máy quét thẻ 03" },
                new MayQuetTheVanTay { MayQuetTheVanTayID = 4, TenMay = "MQT04", MoTa = "Máy quét thẻ 04" },
                new MayQuetTheVanTay { MayQuetTheVanTayID = 5, TenMay = "MQT05", MoTa = "Máy quét thẻ 05" },
                new MayQuetTheVanTay { MayQuetTheVanTayID = 6, TenMay = "MQT06", MoTa = "Máy quét thẻ 06" },
                new MayQuetTheVanTay { MayQuetTheVanTayID = 7, TenMay = "MQT07", MoTa = "Máy quét thẻ 07" },
                new MayQuetTheVanTay { MayQuetTheVanTayID = 8, TenMay = "MQT08", MoTa = "Máy quét thẻ 08" },
                new MayQuetTheVanTay { MayQuetTheVanTayID = 9, TenMay = "MQT09", MoTa = "Máy quét thẻ 09" }
            };

            var suKiens = new List<SuKien>
            {
                new SuKien
                {
                    SuKienID = 1,
                    TenSuKien = "TL005 - 01",
                    MoTa = "Tâm lý học đại cương tuần 01",
                    NgayBatDau = Convert.ToDateTime("3/9/2018 12:00:00 AM"),
                    NgayKetThuc = Convert.ToDateTime("3/9/2018 12:00:00 AM"),
                    ThoiGianBatDau = Convert.ToDateTime("3/9/2018 5:45:00 PM"),
                    ThoiGianKetThuc = Convert.ToDateTime("3/9/2018 8:30:00 PM"),
                    SoNguoiToiDa = 90,
                    SoNguoiThamGia = 6,
                    NgayTao = Convert.ToDateTime("2/9/2018 3:30:00 PM"),
                    NgayCapNhat = Convert.ToDateTime("3/9/2018 8:30:00 PM"),
                    //TrangThai = 1,
                    PhongID = 3,
                    NguoiPhuTrachID = 8
                },
                new SuKien
                {
                    SuKienID = 2,
                    TenSuKien = "TL005 - 02",
                    MoTa = "Tâm lý học đại cương tuần 02",
                    NgayBatDau = Convert.ToDateTime("3/16/2018 12:00:00 AM"),
                    NgayKetThuc = Convert.ToDateTime("3/16/2018 12:00:00 AM"),
                    ThoiGianBatDau = Convert.ToDateTime("3/16/2018 5:45:00 PM"),
                    ThoiGianKetThuc = Convert.ToDateTime("3/16/2018 8:30:00 PM"),
                    SoNguoiToiDa = 90,
                    SoNguoiThamGia = 5,
                    NgayTao = Convert.ToDateTime("2/9/2018 3:30:00 PM"),
                    NgayCapNhat = Convert.ToDateTime("3/16/2018 8:30:00 PM"),
                    //TrangThai = 1,
                    PhongID = 3,
                    NguoiPhuTrachID = 8
                },
                new SuKien
                {
                    SuKienID = 3,
                    TenSuKien = "TL005 - 03",
                    MoTa = "Tâm lý học đại cương tuần 03",
                    NgayBatDau = Convert.ToDateTime("3/23/2018 12:00:00 AM"),
                    NgayKetThuc = Convert.ToDateTime("3/23/2018 12:00:00 AM"),
                    ThoiGianBatDau = Convert.ToDateTime("3/23/2018 5:45:00 PM"),
                    ThoiGianKetThuc = Convert.ToDateTime("3/23/2018 8:30:00 PM"),
                    SoNguoiToiDa = 90,
                    SoNguoiThamGia = 4,
                    NgayTao = Convert.ToDateTime("2/9/2018 3:30:00 PM"),
                    NgayCapNhat = Convert.ToDateTime("3/23/2018 8:30:00 PM"),
                    //TrangThai = 1,
                    PhongID = 3,
                    NguoiPhuTrachID = 8
                },
                new SuKien
                {
                    SuKienID = 4,
                    TenSuKien = "TL005 - 04",
                    MoTa = "Tâm lý học đại cương tuần 04",
                    NgayBatDau = Convert.ToDateTime("3/30/2018 12:00:00 AM"),
                    NgayKetThuc = Convert.ToDateTime("3/30/2018 12:00:00 AM"),
                    ThoiGianBatDau = Convert.ToDateTime("3/30/2018 5:45:00 PM"),
                    ThoiGianKetThuc = Convert.ToDateTime("3/30/2018 8:30:00 PM"),
                    SoNguoiToiDa = 90,
                    SoNguoiThamGia = 5,
                    NgayTao = Convert.ToDateTime("2/9/2018 3:30:00 PM"),
                    NgayCapNhat = Convert.ToDateTime("3/30/2018 8:30:00 PM"),
                    //TrangThai = 1,
                    PhongID = 3,
                    NguoiPhuTrachID = 8
                },
                new SuKien
                {
                    SuKienID = 5,
                    TenSuKien = "AV003 - 01",
                    MoTa = "Anh văn 3 - tuần 01",
                    NgayBatDau = Convert.ToDateTime("3/1/2018 12:00:00 AM"),
                    NgayKetThuc = Convert.ToDateTime("3/1/2018 12:00:00 AM"),
                    ThoiGianBatDau = Convert.ToDateTime("3/1/2018 5:45:00 PM"),
                    ThoiGianKetThuc = Convert.ToDateTime("3/1/2018 8:30:00 PM"),
                    SoNguoiToiDa = 90,
                    SoNguoiThamGia = 7,
                    NgayTao = Convert.ToDateTime("2/9/2018 3:30:00 PM"),
                    NgayCapNhat = Convert.ToDateTime("3/1/2018 8:30:00 PM"),
                    //TrangThai = 1,
                    PhongID = 5,
                    NguoiPhuTrachID = 9
                },
                new SuKien
                {
                    SuKienID = 6,
                    TenSuKien = "AV003 - 02",
                    MoTa = "Anh văn 3 - tuần 02",
                    NgayBatDau = Convert.ToDateTime("3/8/2018 12:00:00 AM"),
                    NgayKetThuc = Convert.ToDateTime("3/8/2018 12:00:00 AM"),
                    ThoiGianBatDau = Convert.ToDateTime("3/8/2018 5:45:00 PM"),
                    ThoiGianKetThuc = Convert.ToDateTime("3/8/2018 8:30:00 PM"),
                    SoNguoiToiDa = 90,
                    SoNguoiThamGia = 7,
                    NgayTao = Convert.ToDateTime("2/9/2018 3:30:00 PM"),
                    NgayCapNhat = Convert.ToDateTime("3/8/2018 8:30:00 PM"),
                    //TrangThai = 1,
                    PhongID = 5,
                    NguoiPhuTrachID = 9
                },
                new SuKien
                {
                    SuKienID = 7,
                    TenSuKien = "AV003 - 03",
                    MoTa = "Anh văn 3 - tuần 03",
                    NgayBatDau = Convert.ToDateTime("3/15/2018 12:00:00 AM"),
                    NgayKetThuc = Convert.ToDateTime("3/15/2018 12:00:00 AM"),
                    ThoiGianBatDau = Convert.ToDateTime("3/15/2018 5:45:00 PM"),
                    ThoiGianKetThuc = Convert.ToDateTime("3/15/2018 8:30:00 PM"),
                    SoNguoiToiDa = 90,
                    SoNguoiThamGia = 10,
                    NgayTao = Convert.ToDateTime("2/9/2018 3:30:00 PM"),
                    NgayCapNhat = Convert.ToDateTime("3/15/2018 8:30:00 PM"),
                    //TrangThai = 1,
                    PhongID = 5,
                    NguoiPhuTrachID = 9
                },
                new SuKien
                {
                    SuKienID = 8,
                    TenSuKien = "AV003 - 04",
                    MoTa = "Anh văn 3 - tuần 04",
                    NgayBatDau = Convert.ToDateTime("3/22/2018 12:00:00 AM"),
                    NgayKetThuc = Convert.ToDateTime("3/22/2018 12:00:00 AM"),
                    ThoiGianBatDau = Convert.ToDateTime("3/22/2018 5:45:00 PM"),
                    ThoiGianKetThuc = Convert.ToDateTime("3/22/2018 8:30:00 PM"),
                    SoNguoiToiDa = 90,
                    SoNguoiThamGia = 10,
                    NgayTao = Convert.ToDateTime("2/9/2018 3:30:00 PM"),
                    NgayCapNhat = Convert.ToDateTime("3/22/2018 8:30:00 PM"),
                    //TrangThai = 1,
                    PhongID = 5,
                    NguoiPhuTrachID = 9
                }
            };

            var thongTinThamGiaSuKiens = new List<ThongTinThamGiaSuKien>
            {
                new ThongTinThamGiaSuKien { SuKienID = 1, NguoiDungID = 3, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/9/2018 5:45:00 PM"), ThoiGianRa = Convert.ToDateTime("3/9/2018 8:30:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 1, NguoiDungID = 4, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/9/2018 5:46:00 PM"), ThoiGianRa = Convert.ToDateTime("3/9/2018 8:31:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 1, NguoiDungID = 5, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 1, NguoiDungID = 6, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/9/2018 5:48:00 PM"), ThoiGianRa = Convert.ToDateTime("3/9/2018 8:33:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 1, NguoiDungID = 7, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/9/2018 5:49:00 PM"), ThoiGianRa = Convert.ToDateTime("3/9/2018 8:34:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 2, NguoiDungID = 3, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/16/2018 5:45:00 PM"), ThoiGianRa = Convert.ToDateTime("3/16/2018 8:30:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 2, NguoiDungID = 4, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/16/2018 5:48:00 PM"), ThoiGianRa = Convert.ToDateTime("3/16/2018 8:33:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 2, NguoiDungID = 5, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 2, NguoiDungID = 6, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/16/2018 5:46:00 PM"), ThoiGianRa = Convert.ToDateTime("3/16/2018 8:31:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 2, NguoiDungID = 7, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/16/2018 5:47:00 PM"), ThoiGianRa = Convert.ToDateTime("3/16/2018 8:32:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 3, NguoiDungID = 3, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/23/2018 5:46:00 PM"), ThoiGianRa = Convert.ToDateTime("3/23/2018 8:31:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 3, NguoiDungID = 4, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/23/2018 5:47:00 PM"), ThoiGianRa = Convert.ToDateTime("3/23/2018 8:32:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 3, NguoiDungID = 5, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/23/2018 5:48:00 PM"), ThoiGianRa = Convert.ToDateTime("3/23/2018 8:33:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 3, NguoiDungID = 6, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/23/2018 5:49:00 PM"), ThoiGianRa = Convert.ToDateTime("3/23/2018 8:34:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 3, NguoiDungID = 7, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 4, NguoiDungID = 3, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/30/2018 5:44:00 PM"), ThoiGianRa = Convert.ToDateTime("3/30/2018 8:29:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 4, NguoiDungID = 4, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/30/2018 5:45:00 PM"), ThoiGianRa = Convert.ToDateTime("3/30/2018 8:30:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 4, NguoiDungID = 5, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/30/2018 5:46:00 PM"), ThoiGianRa = Convert.ToDateTime("3/30/2018 8:31:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 4, NguoiDungID = 6, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/30/2018 5:47:00 PM"), ThoiGianRa = Convert.ToDateTime("3/30/2018 8:33:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 4, NguoiDungID = 7, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 5, NguoiDungID = 3, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/1/2018 5:45:00 PM"), ThoiGianRa = Convert.ToDateTime("3/1/2018 8:30:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 5, NguoiDungID = 4, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/1/2018 5:46:00 PM"), ThoiGianRa = Convert.ToDateTime("3/1/2018 8:31:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 5, NguoiDungID = 5, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/1/2018 5:47:00 PM"), ThoiGianRa = Convert.ToDateTime("3/1/2018 8:32:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 5, NguoiDungID = 6, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 5, NguoiDungID = 7, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/1/2018 5:49:00 PM"), ThoiGianRa = Convert.ToDateTime("3/1/2018 8:34:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 6, NguoiDungID = 3, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/8/2018 5:45:00 PM"), ThoiGianRa = Convert.ToDateTime("3/8/2018 8:30:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 6, NguoiDungID = 4, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/8/2018 5:46:00 PM"), ThoiGianRa = Convert.ToDateTime("3/8/2018 8:31:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 6, NguoiDungID = 5, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 6, NguoiDungID = 6, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/8/2018 5:48:00 PM"), ThoiGianRa = Convert.ToDateTime("3/8/2018 8:33:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 6, NguoiDungID = 7, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/8/2018 5:49:00 PM"), ThoiGianRa = Convert.ToDateTime("3/8/2018 8:34:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 7, NguoiDungID = 3, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/15/2018 5:49:00 PM"), ThoiGianRa = Convert.ToDateTime("3/15/2018 8:34:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 7, NguoiDungID = 4, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 7, NguoiDungID = 5, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/15/2018 5:47:00 PM"), ThoiGianRa = Convert.ToDateTime("3/15/2018 8:32:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 7, NguoiDungID = 6, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/15/2018 5:46:00 PM"), ThoiGianRa = Convert.ToDateTime("3/15/2018 8:31:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 7, NguoiDungID = 7, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 8, NguoiDungID = 3, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/22/2018 5:49:00 PM"), ThoiGianRa = Convert.ToDateTime("3/22/2018 8:34:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 8, NguoiDungID = 4, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/22/2018 5:48:00 PM"), ThoiGianRa = Convert.ToDateTime("3/22/2018 8:33:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 8, NguoiDungID = 5, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/22/2018 5:47:00 PM"), ThoiGianRa = Convert.ToDateTime("3/22/2018 8:32:00 PM") },
                new ThongTinThamGiaSuKien { SuKienID = 8, NguoiDungID = 6, LaGiangVien = false, TrangThai = 0, ThoiGianVao = null, ThoiGianRa = null },
                new ThongTinThamGiaSuKien { SuKienID = 8, NguoiDungID = 7, LaGiangVien = false, TrangThai = 1, ThoiGianVao = Convert.ToDateTime("3/22/2018 5:45:00 PM"), ThoiGianRa = Convert.ToDateTime("3/22/2018 8:30:00 PM") },
            };

            loaiNguoiDungs.ForEach(s => context.LoaiNguoiDungs.AddOrUpdate(p => p.LoaiNguoiDungID, s));
            nguoiDungs.ForEach(s => context.NguoiDungs.AddOrUpdate(p => p.NguoiDungID, s));
            theTus.ForEach(s => context.TheTus.AddOrUpdate(p => p.TheTuID, s));
            phongs.ForEach(s => context.Phongs.AddOrUpdate(p => p.PhongID, s));
            mays.ForEach(s => context.MayQuetTheVanTays.AddOrUpdate(p => p.MayQuetTheVanTayID, s));
            suKiens.ForEach(s => context.SuKiens.AddOrUpdate(p => p.SuKienID, s));
            thongTinThamGiaSuKiens.ForEach(s => context.ThongTinThamGiaSuKiens.AddOrUpdate(p => new { p.SuKienID, p.NguoiDungID }, s));
            context.SaveChanges();
        }
    }
}
