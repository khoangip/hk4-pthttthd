namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeDateTime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SuKien", "NgayTao", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SuKien", "NgayCapNhat", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.NguoiDung", "NgaySinh", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.NguoiDung", "NgayTao", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.NguoiDung", "NgayCapNhat", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.TheTu", "NgayTao", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.TheTu", "NgayCapNhat", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TheTu", "NgayCapNhat", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TheTu", "NgayTao", c => c.DateTime(nullable: false));
            AlterColumn("dbo.NguoiDung", "NgayCapNhat", c => c.DateTime(nullable: false));
            AlterColumn("dbo.NguoiDung", "NgayTao", c => c.DateTime(nullable: false));
            AlterColumn("dbo.NguoiDung", "NgaySinh", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SuKien", "NgayCapNhat", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SuKien", "NgayTao", c => c.DateTime(nullable: false));
        }
    }
}
