namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeTrangThaiToBoolInTheTu : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TheTu", "TrangThai", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TheTu", "TrangThai", c => c.Int(nullable: false));
        }
    }
}
