// <auto-generated />
namespace DiemDanhVanTay.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addThoiGianVaoThoiGianRa : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addThoiGianVaoThoiGianRa));
        
        string IMigrationMetadata.Id
        {
            get { return "201805161257462_addThoiGianVaoThoiGianRa"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
