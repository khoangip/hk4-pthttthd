namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNguoiPhuTrach : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SuKien", "NguoiPhuTrachID", c => c.Int());
            CreateIndex("dbo.SuKien", "NguoiPhuTrachID");
            AddForeignKey("dbo.SuKien", "NguoiPhuTrachID", "dbo.NguoiDung", "NguoiDungID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SuKien", "NguoiPhuTrachID", "dbo.NguoiDung");
            DropIndex("dbo.SuKien", new[] { "NguoiPhuTrachID" });
            DropColumn("dbo.SuKien", "NguoiPhuTrachID");
        }
    }
}
