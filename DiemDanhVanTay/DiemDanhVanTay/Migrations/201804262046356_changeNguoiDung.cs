namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeNguoiDung : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.NguoiDung", "MSSV", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.NguoiDung", "MSSV", c => c.String(nullable: false));
        }
    }
}
