namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeTrangThaiToBool : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.NguoiDung", "TrangThai", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.NguoiDung", "TrangThai", c => c.Int());
        }
    }
}
