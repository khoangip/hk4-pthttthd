namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPhongInSuKien : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SuKien", "PhongID", c => c.Int(nullable: false));
            CreateIndex("dbo.SuKien", "PhongID");
            AddForeignKey("dbo.SuKien", "PhongID", "dbo.Phong", "PhongID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SuKien", "PhongID", "dbo.Phong");
            DropIndex("dbo.SuKien", new[] { "PhongID" });
            DropColumn("dbo.SuKien", "PhongID");
        }
    }
}
