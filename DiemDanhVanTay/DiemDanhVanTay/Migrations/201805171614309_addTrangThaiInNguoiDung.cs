namespace DiemDanhVanTay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTrangThaiInNguoiDung : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NguoiDung", "TrangThai", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.NguoiDung", "TrangThai");
        }
    }
}
