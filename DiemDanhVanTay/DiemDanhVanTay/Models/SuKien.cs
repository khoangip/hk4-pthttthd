﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiemDanhVanTay.Models
{
    public class SuKien
    {
        [Key]
        public int SuKienID { get; set; }
        [Required]
        [Display(Name = "Tên sự kiện")]
        public string TenSuKien { get; set; }


        [Display(Name = "Mô tả")]
        public string MoTa { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date)]
        [Display(Name = "Ngày bắt đầu")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime NgayBatDau { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date)]
        [Display(Name = "Ngày kết thúc")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime NgayKetThuc { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.Time)]
        [Display(Name = "Thời gian bắt đầu")]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime ThoiGianBatDau { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.Time)]
        [Display(Name = "Thời gian kết thúc")]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime ThoiGianKetThuc { get; set; }

        [Display(Name = "Số người tối đa")]
        public int SoNguoiToiDa { get; set; }

        [Display(Name = "Số người tham gia")]
        public int SoNguoiThamGia { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày tạo")]
        public DateTime NgayTao { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày cập nhật")]
        public DateTime NgayCapNhat { get; set; }

        //[Display(Name = "Trạng thái")]
        //public int TrangThai { get; set; }

        [ForeignKey("Phong")]
        [Display(Name = "Phòng")]
        public int PhongID { get; set; }

        [ForeignKey("NguoiDung")]
        [Display(Name = "Người phụ trách")]
        public int? NguoiPhuTrachID { get; set; }

        public virtual Phong Phong { get; set; }

        public virtual NguoiDung NguoiDung { get; set; }
    }
}