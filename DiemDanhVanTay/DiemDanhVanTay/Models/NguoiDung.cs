﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiemDanhVanTay.Models
{
    public class NguoiDung
    {
        [Key]
        public int NguoiDungID { get; set; }

        [ForeignKey("LoaiNguoiDung")]
        [Display(Name ="Loại người dùng")]
        public int LoaiNguoiDungID { get; set; }

        public string MSSV { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string MatKhau { get; set; }

        [Required]
        [Display(Name = "Họ tên")]
        [StringLength(50, MinimumLength = 4)]
        public string HoTen { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date)]
        [Display(Name = "Ngày sinh")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime NgaySinh { get; set; }

        [Required]
        [Display(Name = "Căn cước/CMND")]
        public string CMND { get; set; }

        [Required]
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }

        [Display(Name = "Điện thoại")]
        [Phone(ErrorMessage = "Invalid Phone Number")]
        public string DienThoai { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày tạo")]
        public DateTime NgayTao { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày cập nhật")]
        public DateTime NgayCapNhat { get; set; }

        [Display(Name = "Trạng thái")]
        public bool TrangThai { get; set; }

        public virtual LoaiNguoiDung LoaiNguoiDung { get; set; }
    }
}