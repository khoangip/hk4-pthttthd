﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.Models
{
    public class ThongTinThamGiaSuKien
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("SuKien")]
        [Display(Name ="Sự kiện")]
        public int SuKienID { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("NguoiDung")]
        [Display(Name = "Người dùng")]
        public int NguoiDungID { get; set; }

        [Required]
        [Display(Name = "Trạng thái")]
        public int TrangThai { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Display(Name = "Thời gian vào")]
        public DateTime? ThoiGianVao { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Display(Name = "Thời gian ra")]
        public DateTime? ThoiGianRa { get; set; }

        [Required]
        [Display(Name = "Là giảng viên")]
        public bool LaGiangVien { get; set; }

        public virtual SuKien SuKien { get; set; }

        public virtual NguoiDung NguoiDung { get; set; }
    }
}