﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiemDanhVanTay.Models
{
    public class LoaiNguoiDung
    {
        [Key]
        public int LoaiNguoiDungID { get; set; }

        [Required]
        [Display(Name ="Tên Loại")]
        public string TenLoaiNguoiDung { get; set; }

        [Required]
        [Display(Name = "Trạng Thái")]
        public int TrangThai { get; set; }
    }
}