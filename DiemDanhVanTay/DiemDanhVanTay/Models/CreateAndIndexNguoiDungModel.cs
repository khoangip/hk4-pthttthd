﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.Models
{
    public class CreateAndIndexNguoiDungModel
    {
        public NguoiDung NguoiDung { get; set; }
        public IEnumerable<NguoiDung> NguoiDungs { get; set; }
    }
}