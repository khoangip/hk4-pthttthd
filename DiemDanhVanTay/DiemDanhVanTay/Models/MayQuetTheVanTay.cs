﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiemDanhVanTay.Models
{
    public class MayQuetTheVanTay
    {
        [Key]
        public int MayQuetTheVanTayID { get; set; }

        [Required]
        [Display(Name = "Tên máy")]
        public string TenMay { get; set; }

        [Display(Name = "Mô tả")]
        public string MoTa { get; set; }
    }
}