﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiemDanhVanTay.Models
{
    public class MaMayThamGiaSuKien
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("SuKien")]
        [Display(Name = "Sự kiện")]
        public int SuKienID { get; set; }

        [Key]
        [ForeignKey("MayQuetTheVanTay")]
        [Column(Order = 1)]
        [Display(Name = "Máy quét")]
        public int MayQuetTheVanTayID { get; set; }

        [Required]
        [Display(Name = "Trạng thái")]
        public int TrangThai { get; set; }

        public virtual MayQuetTheVanTay MayQuetTheVanTay { get; set; }

        public virtual SuKien SuKien { get; set; }
    }
}