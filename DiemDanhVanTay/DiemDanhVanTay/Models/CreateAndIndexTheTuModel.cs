﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.Models
{
    public class CreateAndIndexTheTuModel
    {
        public TheTu TheTu { get; set; }
        public IEnumerable<TheTu> TheTus { get; set; }
    }
}