﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DiemDanhVanTay.Models
{
    public class TheTu
    {
        [Key]
        public int TheTuID { get; set; }

        [ForeignKey("NguoiDung")]
        [Display(Name = "Người dùng")]
        public int NguoiDungID { get; set; }

        [Display(Name = "Vân tay thứ nhất")]
        public string VanTay1 { get; set; }

        [Display(Name = "Vân tay thứ hai")]
        public string VanTay2 { get; set; }

        [Display(Name = "Vân tay thứ ba")]
        public string VanTay3 { get; set; }

        [Required]
        [Display(Name = "Trạng thái")]
        public bool TrangThai { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Ngày tạo")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime NgayTao { get; set; }

        [Column(TypeName = "datetime2")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Ngày cập nhật")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime NgayCapNhat { get; set; }

        public virtual NguoiDung NguoiDung { get; set; }
    }
}