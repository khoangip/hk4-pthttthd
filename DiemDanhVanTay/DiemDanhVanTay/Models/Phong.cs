﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DiemDanhVanTay.Models
{
    public class Phong
    {
        [Key]
        public int PhongID { get; set; }

        [Required]
        [Display(Name = "Tên phòng")]
        public string TenPhong { get; set; }

        [Required]
        [Display(Name = "Số chỗ ngồi")]
        public int SoChoNgoi { get; set; }
    }
}