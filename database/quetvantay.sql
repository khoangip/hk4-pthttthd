USE [quetvantay]
GO
/****** Object:  Table [dbo].[loainguoidung]    Script Date: 13/04/2018 11:16:17 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[loainguoidung](
	[MaLoaiNguoiDung] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiNguoiDung] [nvarchar](255) NOT NULL,
	[TrangThai] [tinyint] NOT NULL,
 CONSTRAINT [PK_loainguoidung] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNguoiDung] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mamaythamgiasukien]    Script Date: 13/04/2018 11:16:17 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mamaythamgiasukien](
	[MaMay] [int] NOT NULL,
	[MaSuKien] [int] NOT NULL,
	[TrangThai] [tinyint] NOT NULL,
 CONSTRAINT [PK_mamaythamgiasukien] PRIMARY KEY CLUSTERED 
(
	[MaMay] ASC,
	[MaSuKien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mayquetthevantay]    Script Date: 13/04/2018 11:16:17 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mayquetthevantay](
	[MaMay] [int] IDENTITY(1,1) NOT NULL,
	[TenMay] [nvarchar](255) NOT NULL,
	[MoTa] [ntext] NOT NULL,
 CONSTRAINT [PK_mayquetthevantay] PRIMARY KEY CLUSTERED 
(
	[MaMay] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[nguoidung]    Script Date: 13/04/2018 11:16:17 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nguoidung](
	[MaNguoiDung] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiNguoiDung] [int] NOT NULL,
	[HoTen] [nvarchar](255) NOT NULL,
	[NgaySinh] [date] NOT NULL,
	[CMND] [varchar](20) NOT NULL,
	[DienThoai] [varchar](20) NULL,
	[Email] [varchar](255) NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[MaNguoiDung] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sukien]    Script Date: 13/04/2018 11:16:17 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sukien](
	[MaSuKien] [int] IDENTITY(1,1) NOT NULL,
	[TenSuKien] [nvarchar](255) NOT NULL,
	[MoTa] [ntext] NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
	[ThoiGianBatDau] [datetime] NULL,
	[ThoiGianKetThuc] [datetime] NULL,
	[SoNguoiToiDa] [int] NULL,
	[SoNguoiThamGia] [int] NULL,
	[TrangThai] [tinyint] NULL,
 CONSTRAINT [PK_sukien] PRIMARY KEY CLUSTERED 
(
	[MaSuKien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[thetu]    Script Date: 13/04/2018 11:16:17 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[thetu](
	[MaThe] [int] IDENTITY(1,1) NOT NULL,
	[MaNguoiDung] [int] NOT NULL,
	[VanTay1] [nvarchar](255) NULL,
	[VanTay2] [nvarchar](255) NULL,
	[VanTay3] [nvarchar](255) NULL,
	[TrangThai] [tinyint] NOT NULL,
	[NgayTao] [datetime] NULL,
	[NgayCapNhat] [datetime] NULL,
 CONSTRAINT [PK_thetu] PRIMARY KEY CLUSTERED 
(
	[MaThe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[thongtinthamgiasukien]    Script Date: 13/04/2018 11:16:17 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[thongtinthamgiasukien](
	[MaSuKien] [int] NOT NULL,
	[MaNguoiDung] [int] NOT NULL,
	[TrangThai] [tinyint] NOT NULL,
	[ThongTinThamGia] [tinyint] NOT NULL,
 CONSTRAINT [PK_thongtinthamgiasukien] PRIMARY KEY CLUSTERED 
(
	[MaSuKien] ASC,
	[MaNguoiDung] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[mamaythamgiasukien]  WITH CHECK ADD  CONSTRAINT [FK_mamaythamgiasukien_mayquetthevantay] FOREIGN KEY([MaMay])
REFERENCES [dbo].[mayquetthevantay] ([MaMay])
GO
ALTER TABLE [dbo].[mamaythamgiasukien] CHECK CONSTRAINT [FK_mamaythamgiasukien_mayquetthevantay]
GO
ALTER TABLE [dbo].[mamaythamgiasukien]  WITH CHECK ADD  CONSTRAINT [FK_mamaythamgiasukien_sukien] FOREIGN KEY([MaSuKien])
REFERENCES [dbo].[sukien] ([MaSuKien])
GO
ALTER TABLE [dbo].[mamaythamgiasukien] CHECK CONSTRAINT [FK_mamaythamgiasukien_sukien]
GO
ALTER TABLE [dbo].[nguoidung]  WITH CHECK ADD  CONSTRAINT [FK_nguoidung_loainguoidung] FOREIGN KEY([MaLoaiNguoiDung])
REFERENCES [dbo].[loainguoidung] ([MaLoaiNguoiDung])
GO
ALTER TABLE [dbo].[nguoidung] CHECK CONSTRAINT [FK_nguoidung_loainguoidung]
GO
ALTER TABLE [dbo].[thetu]  WITH CHECK ADD  CONSTRAINT [FK_thetu_nguoidung] FOREIGN KEY([MaNguoiDung])
REFERENCES [dbo].[nguoidung] ([MaNguoiDung])
GO
ALTER TABLE [dbo].[thetu] CHECK CONSTRAINT [FK_thetu_nguoidung]
GO
ALTER TABLE [dbo].[thongtinthamgiasukien]  WITH CHECK ADD  CONSTRAINT [FK_thongtinthamgiasukien_nguoidung] FOREIGN KEY([MaNguoiDung])
REFERENCES [dbo].[nguoidung] ([MaNguoiDung])
GO
ALTER TABLE [dbo].[thongtinthamgiasukien] CHECK CONSTRAINT [FK_thongtinthamgiasukien_nguoidung]
GO
ALTER TABLE [dbo].[thongtinthamgiasukien]  WITH CHECK ADD  CONSTRAINT [FK_thongtinthamgiasukien_sukien] FOREIGN KEY([MaSuKien])
REFERENCES [dbo].[sukien] ([MaSuKien])
GO
ALTER TABLE [dbo].[thongtinthamgiasukien] CHECK CONSTRAINT [FK_thongtinthamgiasukien_sukien]
GO
